<?php
	error_reporting(E_ALL);
	/**
	 * Define some useful constants
	 */
	define('BASE_DIR', dirname(__DIR__));
	define('MODULE_DIR',BASE_DIR.'/modules');

	require_once(BASE_DIR.'/core/Shared.php');
	require_once(BASE_DIR.'/core/ModuleTemplate.php');

	use \Phalcon\Mvc\Router,
	\Phalcon\Mvc\Application,
	\Phalcon\DI\FactoryDefault,
	\Phalcon\Mvc\Url,
	\Phalcon\Loader,
	\Phalcon\Mvc\Model,
	\Core\Shared,
	\Phalcon\Cache\Frontend\Output as OutputFrontend,
	\Phalcon\Cache\Backend\File as FileBackend;

	//Disable notNull validation for models so we can save empty values in a sane matter
	$aSetup = array(
		'notNullValidations' => false
	);
	Model::setup($aSetup);

	/**
	 * Try to read the config file and throw an error if it can't be loaded
	 */
	try {
		$aConfig = include BASE_DIR.'/config/Config.php';
	} catch(\Exception $e){
		echo $e->getMessage();
		die;
	}

	//Save the config to the DependencyInjector
	$oDi = new FactoryDefault();
	$oDi->set('config',$aConfig);

	//Set the global url prefix, '/' for normal sites
	$oDi->set('url', function() {
		$url = new \Phalcon\Mvc\Url();
		$url->setBaseUri('/');
		return $url;
	}, true);

	//Still need to test and see what use this has
	if(2 == 3) {
		$oDi->set('modelsManager', function() {
			return new Phalcon\Mvc\Model\Manager();
		});
	}
	//Start the session the first time when some component request the session service
	$oDi->setShared('session', function() {
		$session = new \Phalcon\Session\Adapter\Files();
		$session->start();
		return $session;
	});

	//The modules to load
	$aModuleNames = array(
		'page',
		'morrowind',
		//'pizza',
		//'arma',
		'login',
		'user',
		'l10n',
		'spice',
		/*'form',*/
		'vimeo',
		'media'
	);

	$oDi->setShared('modules',$aModuleNames);

	$aModules = Shared::buildModuleArray($aModuleNames);
	$aRouteGroups = Shared::loadRouteGroups($aModuleNames);

	$oRouter = new Router();
	$oRouter->removeExtraSlashes(true);

	foreach($aRouteGroups AS $oRouteGroup) {
		$oRouter->mount($oRouteGroup);
	}
	$oRouter->setDefaultModule('page');

	//Specify routes for modules
	$oDi->set('router', $oRouter);

	// Set the views cache service
	$oDi->set('viewCache', function () {

		// Cache data for one day by default
		$frontCache = new OutputFrontend(
			array(
				'lifetime' => 3600
			)
		);

		// Memcached connection settings
		$cache = new FileBackend(
			$frontCache,
			array(
				'prefix' => 'cache-',
				'cacheDir' => BASE_DIR.'/cache/'
			)
		);

		return $cache;
	}, false);

	//Debugging stuff
	if(2 == 3) {
		$sUrl = $_SERVER['REQUEST_URI'];
		$router = $oRouter;
		$router->handle($sUrl);
		$router->setUriSource(\Phalcon\Mvc\Router::URI_SOURCE_SERVER_REQUEST_URI);

		/** @var $matched \Phalcon\Mvc\Router\Route */
		$matched = $router->getMatchedRoute();
		$paths = $matched->getPaths();

		print_r($matched);
		die;
	}

	try {
		$eventsManager = new \Phalcon\Events\Manager();
		$loader = new Loader();
		//Debugging stuff for linux paths, case sensitive buggers
		if(2 ==3 ) {
			//Listen all the loader events
			$eventsManager->attach('loader', function($event, $loader) {
				if ($event->getType() == 'beforeCheckPath') {
					echo $loader->getCheckedPath().'<br />';
				}
				if($event->getType() == 'afterCheckPath') {
					echo('path not found');
					die;
				}
			});

			$loader->setEventsManager($eventsManager);
		}
		//Build and register the module namespaces for auto-loading
		$aNamespaces = Shared::buildNamespaces($aModuleNames);
		$loader->registerNamespaces($aNamespaces);
		$loader->register();

		//Create an application
		$oApplication = new Application($oDi);
		// Register the installed modules
		$oApplication->registerModules($aModules);
		//Disable the automatic rendering of views
		$oApplication->useImplicitView(false);
		if($aConfig->firstRun === true) {
			Shared::registerModules($aModuleNames);
		}
		//Handle the request
		echo $oApplication->handle()->getContent();

	} catch(\Exception $e){
		echo('Exception: ');
		echo $e->getMessage();
	}
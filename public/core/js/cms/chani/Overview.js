//Class for Overview functionality
//TODO: IS THIS STILL IN USE?
var chaniOverview = new Class({
	Implements: Options,

	options: {
		rowsParent: 'overviewRows',
		changeSequenceUrl: '',
		refreshOverviewUrl: '',
		demo: false
	},

	initialize: function(options) {

		this.moving = false;
		this.movingEl = false;
		this.movingGhost = false;

		this.setOptions(options);

		this.assignEvents();
	},

	assignEvents: function() {
		//Check if the overview parent exists
		this.rowsParent = $(this.options.rowsParent);
		if(this.rowsParent) {

			var deleteIcons = this.rowsParent.getElements('.delete a');
			for(var i = 0; i < deleteIcons.length; i++) {
				deleteIcons[i].addEvent('click',function() {
					var txt = this.getAttribute('data-confirm');
					return confirm(txt);
				});
			}

			//Get the move elements and assign the event that will register which element the user wants to move
			//Clicking on a move icon again will cancel the moving
			this.rows = this.rowsParent.getElements('.move');
			for(var i = 0; i < this.rows.length; i++) {
				this.rows[i].addEvent('click', function(el, event) {
					event.preventDefault();
					if(this.moving === false) {
						this.registerEl(el);
					} else {
						this.clearRegisteredEl();
					}
					return false;
				}.bind(this, this.rows[i]));
			}

			this.parentPos = this.rowsParent.getPosition();

			//Create a function to handle the mouse move event
			//And store a reference to add / remove the event depending on user actions
			this.mouseMove = function(event) {
				//Update the position of the ghost element to follow the mouse
				//The offset is defined in CSS by margin-left / top
				if(this.movingGhost) {
					this.movingGhost.setStyle('top',event.page.y - this.parentPos.y);
					this.movingGhost.setStyle('left',event.page.x - this.parentPos.x);
				}
			};

			//Add the events to the clickable areas that determine which element is being moved where
			//And to what position
			this.clickAreas = this.rowsParent.getElements('.click');
			for(var i = 0; i < this.clickAreas.length; i++) {

				this.clickAreas[i].addEvent('click',function(el, event) {
					event.preventDefault();
					//The id of the element being moved
					var aIds = this.movingEl.parentNode.parentNode.id.split('_');
					var id = aIds[1];

					//The parent id of the element the user click
					//Top level defaults to 0
					var iParentId = 0;

					//See if there's an actual parent id
					var ul = el.parentNode.parentNode.parentNode;
					var ulIds = ul.id.split('_');
					if(ulIds.length > 1) {
						iParentId = ulIds[1];
					}

					// -1 indicates we're adding the element to the bottom of the (sub-)list
					// This is changed if a 'real' target row has been found
					var iTargetId = -1;

					//The element the user clicked on, the element being moved will be placed above this one
					if(el.parentNode.parentNode.hasClass('newRow') === false) {
						aIds = el.parentNode.parentNode.id.split('_');
						iTargetId = aIds[1];
					}

					//Clear the stored and ghost elements, remove the mousemove function
					this.clearRegisteredEl();

					//Change the sequence through AJAX - updating the overview is handled from there
					console.log('calling change sequence');
					this.changeSequence(id, iTargetId, iParentId);
					return false;
				}.bind(this, this.clickAreas[i]));

				this.clickAreas[i].store('fx', new Fx.Tween(this.clickAreas[i],{
					property: 'opacity',
					duration: 200
				}));

				this.clickAreas[i].addEvent('mouseenter',function() {
					var tween = this.retrieve('fx');
					tween.start(0.1);
				});

				this.clickAreas[i].addEvent('mouseleave',function() {
					var tween = this.retrieve('fx');
					tween.start(1);
				});


			}

			//Collapse the sub rows
			this.uls = this.rowsParent.getElements('li ul');
			for(var i = 0; i < this.uls.length; i++) {
				//More than one child li means there are actual items in the list
				//Otherwise it's just the click placeholder
				if(this.uls[i].children.length > 1) {

					this.uls[i].store('height', this.uls[i].getDimensions().y);
					var tween = new Fx.Tween(this.uls[i],{
						property: 'height',
						duration: 500,
						onComplete: function() {
							if(this.element.getStyle('height').toInt() != 0) {
								this.element.setStyle('height','auto');
							}
						}
					});
					this.uls[i].store('tween', tween);

					var el = this.uls[i].parentNode;
					var list = $$('#' + el.id + ' > div .list')[0];
					el.addClass('collapsed');
					//Make the entire bar clickable for the collapese event and check wether the element that was clicked
					//was either the bar or the list icon
					list.parentNode.addEvent('click',function(list, event) {
						if(event.target.parentNode === list || event.target === list.parentNode) {
							event.preventDefault();
							var el = list.parentNode.parentNode;
							var tween = this.retrieve('tween');
							var i = list.getElement('i');
							if(el.hasClass('collapsed')) {
								i.removeClass('fa-folder');
								i.addClass('fa-folder-open');
								tween.element.setStyles({
									'opacity': 0,
									'height': 'auto'
								});
								var height = tween.element.getDimensions().y;
								this.store('height', height);
								tween.set(0);
								tween.element.setStyle('opacity',1);
								el.removeClass('collapsed');
								tween.start(height);
							} else {
								i.removeClass('fa-folder-open');
								i.addClass('fa-folder');
								el.addClass('collapsed');
								this.store('height', this.getDimensions().y);
								tween.set(this.retrieve('height'));
								tween.start(0);
							}
							return false;
						} else {
							return true;
						}
					}.bind(this.uls[i], list));
					if(el.hasClass('collapsed')) {
						tween.set(0);
					}
				}

			}

			this.rowsParent.removeClass('refreshing');
		}
	},

	changeSequence: function(id, iTargetId, iParentId) {

		console.log('change sequence');
		return false;
		//Change the sequence of an element and refresh the view afterwards
		var req = new Request({
			url: this.options.changeSequenceUrl,
			data: {
				id: id,
				iTargetId: iTargetId,
				iParentId: iParentId
			},
			onSuccess: function(txt) {
				if(txt == 'success') {
					this.refreshOverview();
				} else {
					alert(txt);
				}
			}.bind(this)
		}).send();
	},

	refreshOverview: function() {
		this.rowsParent.addClass('refreshing');
		//Request an updated overview
		var req = new Request({
			url: this.options.refreshOverviewUrl,
			onSuccess: function(txt) {
				var div = document.createElement('div');
				div.id = 'overviewRows';
				div.className = 'overview';
				div.innerHTML = txt;

				var fragment = new DocumentFragment();
				fragment.appendChild(div);
				//Update the overview with 500 ms delay to allow any CSS transitions to complete first and avoid
				//jerky movement / behaviour
				this.replaceOverview.delay(500, this, fragment);
			}.bind(this)
		}).send();
	},
	replaceOverview: function(fragment) {
		this.rowsParent.parentNode.replaceChild(fragment, this.rowsParent);
		this.assignEvents();
	},
	registerEl: function(el) {
		this.addOrder();
		el.parentNode.parentNode.addClass('moving');
		this.movingEl = el;
		var ghostId = this.rowsParent.id + 'Ghost';
		var ghost = el.parentNode.clone();
		ghost.id = ghostId;
		ghost.setStyle('opacity',0.75);
		ghost.setStyle('position','absolute');
		ghost.addClass('ghost');
		this.rowsParent.appendChild(ghost);
		this.moving = true;
		this.movingGhost = $(ghostId);

		this.rowsParent.addEvent('mousemove', this.mouseMove.bind(this));

	},

	getRegisteredEl: function() {
		return this.movingEl;
	},

	clearRegisteredEl: function() {

		this.rowsParent.removeEvent('mousemove', this.mouseMove);

		if(this.movingEl) {
			this.movingEl.parentNode.parentNode.removeClass('moving');
		}

		this.movingEl = false;

		if(this.movingGhost) {
			this.movingGhost.parentNode.removeChild(this.movingGhost);
			this.movingGhost = false;
		}

		this.removeOrder();
		this.moving = false;

	},

	addOrder: function() {
		this.rowsParent.addClass('order');
	},

	removeOrder: function() {
		this.rowsParent.removeClass('order');
	}
});

var chaniCrop = new Class({
	Implements: Options,

	options: {
		demo: false,
		backgroundOverlay   : true,
		overlayColor        : '#333333',
		cropperWidth        : 640,
		cropperHeight       : 480,
		saveMediaUrl        : '/chani/media/saveCrop',
		resetCropUrl        : '/chani/media/resetCrop',
		removeCropUrl       : ''
	},
	initialize: function(options) {

		this.setOptions(options);

		//Add click events to all images that need a cropper
		var aEls = $$('.cropper');
		this.assignClicks(aEls);

		if(this.options.demo == true) {
			var el = $('chaniImageCropperReset');
			if(el) {
				el.addEvent('click',function(el) {
					this.resetCrop();
				}.bind(this,el));
				this.resetButton = el;
			}
			el = $('chaniImageCropperRemove');
			if(el) {
				this.removeButton = el;
				el.addEvent('click',function(el) {
					this.removeDemoCrop();
				}.bind(this,el));
			}
		}

	},
	assignClicks: function(aEls) {
		for(var i = 0 ; i < aEls.length; i++) {
			this.assignClick(aEls[i]);
		}
	},
	assignClick: function(el) {
		//Assign the click event to load the image into the cropper
		el.addEvent('click', function(el, event) {
			var thumb = $('media_' + el.getAttribute('data-id'));
			this.loadFromButton(thumb);
		}.bind(this,el));
	},
	loadFromButton: function(el) {
		var thumb = $('media_' + el.getAttribute('data-id'));
		var settings = this.getSettings(thumb);
		this.loadImage(settings);
	},
	getSettings: function(el) {
		var settings = {};

		if(el.hasAttribute('data-scale')) {
			settings.scale = el.getAttribute('data-scale').toInt();
		} else {
			settings.scale = 100;
		}

		if(el.hasAttribute('data-cropper-width')) {
			settings.cropperWidth = el.getAttribute('data-cropper-width').toInt();
		} else {
			settings.cropperWidth = this.options.cropperWidth;
		}

		if(el.hasAttribute('data-cropper-height')) {
			settings.cropperHeight = el.getAttribute('data-cropper-height').toInt();
		} else {
			settings.cropperHeight = this.options.cropperHeight;
		}

		if(el.hasAttribute('data-crop-x')) {
			settings.cropX = el.getAttribute('data-crop-x').toInt();
		} else {
			settings.cropX = 0;
		}

		if(el.hasAttribute('data-crop-y')) {
			settings.cropY = el.getAttribute('data-crop-y').toInt();
		} else {
			settings.cropY = 0;
		}

		if(el.hasAttribute('data-crop-width')) {
			settings.cropWidth = el.getAttribute('data-crop-width').toInt();
		} else {
			settings.cropWidth = 0;
		}

		if(el.hasAttribute('data-crop-height')) {
			settings.cropHeight = el.getAttribute('data-crop-height').toInt();
		} else {
			settings.cropHeight = 0;
		}

		if(el.hasAttribute('data-border')) {
			var border = el.getAttribute('border').split(' ');
			settings.borderTopWidth = border[0].toInt();
			settings.borderRightWidth = border[1].toInt();
			settings.borderBottomWidth = border[2].toInt();
			settings.borderLeftWidth = border[3].toInt();
		} else {
			settings.borderTopWidth = 8;
			settings.borderRightWidth = 8;
			settings.borderBottomWidth = 8;
			settings.borderLeftWidth = 8;
		}
		settings.mediaId = el.getAttribute('data-id');
		console.log(el);
		settings.url = el.getAttribute('data-url');
		//var iMediaId = el.getAttribute('data-id');
		//var sUrl = el.getAttribute('data-url');

		this.targetEl = el;
		this.targetPosition = el.getBoundingClientRect();
		this.targetPositionBody = el.getPosition(document.body);
		this.targetDimensions = el.getDimensions();
		return settings;
	},
	loadImage: function(settings) {
		//Load the cropper settings and create it
		this.iMediaId = settings.mediaId;
		this.sUrl = settings.url;
		this.settings = settings;
		this.createCropper();
	},

	createCropper: function() {

		var documentFragment = new DocumentFragment();
		//We only create the cropper once and store it upon removal from the DOM
		if(!this.cropper) {
			var fx;
			if(this.options.backgroundOverlay == true) {

				var background = new Element('div',{
					id: 'chaniCropOverlay'
				});

				fx = new Fx.Morph(background, {
					duration: 1,
					transition: 'sine:in',
					onComplete: function() {
						if(this.removing == true) {
							this.backgroundEl.setStyles({
								top: 0,
								left: 0,
								width: 0,
								height: 0
							});
							this.backgroundEl = document.body.removeChild(this.backgroundEl);
						}
					}.bind(this)
				});

				background.store('fx',fx);
				background.setStyle('backgroundColor','#444444');
				background.setStyle('position','absolute');
				background.addEvent('click',function() {
					this.removeCropper();
				}.bind(this));

				this.backgroundEl = documentFragment.appendChild(background);

			}

			var wrapper = new Element('div',{
				id: 'chaniCrop',
				class: 'chaniCropWrapper'
			});

			wrapper.setStyle('position','fixed');
			wrapper.store('settings',this.settings);

			var UI = new Element('div',{
				id: 'chaniCropUI',
				class: 'chaniCropUI'
			});

			var cropControls = new Element('div',{
				id: 'chaniCropControls',
				class: 'chaniCropControls'
			});

			var slider = new Element('div',{
				id: 'chaniCropSlider',
				class: 'chaniCropslider'
			});

			var knob = new Element('div',{
				id: 'chaniCropSliderKnob',
				class: 'chaniCropSliderKnob'
			});

			this.knob = slider.appendChild(knob);
			this.slider = cropControls.appendChild(slider);

			var percentageEl = new Element('div',{
				class: 'chaniCropPercentage'
			});

			var percentageLabel = new Element('label');
			percentageLabel.innerHTML = 'Percentage:';
			percentageEl.appendChild(percentageLabel);

			var percentage = new Element('span');
			percentage.innerHTML = this.settings.scale + '%';
			this.percentage = percentageEl.appendChild(percentage);

			cropControls.appendChild(percentageEl);

			this.cropControls = UI.appendChild(cropControls);

			var info = new Element('div',{
				id: 'chaniCropInfo',
				class: 'chaniCropInfo'
			});
			fx = new Fx.Tween(info,{
				property: 'opacity',
				duration: 1
			});
			fx.set(0);
			info.store('fx',fx);

			var imageInfoEl = new Element('div',{
				class: 'imageInfo'
			});

			var imageLabel = new Element('label');
			imageLabel.innerHTML = 'Image:';
			imageInfoEl.appendChild(imageLabel);

			var imageWidth = new Element('span');
			this.imageWidth = imageInfoEl.appendChild(imageWidth);
			this.imageWidth.title = 'Current image width';
			this.imageWidth.innerHTML = 'bla';

			var imageTimes = new Element('span');
			this.imageTimes = imageInfoEl.appendChild(imageTimes);
			this.imageTimes.innerHTML = 'x';

			var imageHeight = new Element('span');
			this.imageHeight = imageInfoEl.appendChild(imageHeight);
			this.imageHeight.title = 'Current image height';
			this.imageHeight.innerHTML = 'bla';

			info.appendChild(imageInfoEl);

			var cropInfoEl = new Element('div',{
				class: 'cropInfo'
			});

			var cropLabel = new Element('label');
			cropLabel.innerHTML = 'Crop:';
			cropInfoEl.appendChild(cropLabel);

			var cropWidth = new Element('span');
			this.cropWidth = cropInfoEl.appendChild(cropWidth);
			this.cropWidth.title = 'Crop width';

			var cropTimes = new Element('span');
			this.cropTimes = cropInfoEl.appendChild(cropTimes);
			this.cropTimes.innerHTML = 'x';

			var cropHeight = new Element('span');
			this.cropHeight = cropInfoEl.appendChild(cropHeight);
			this.cropHeight.title = 'Crop height';

			var cropTimes = new Element('span');
			cropTimes.innerHTML = '|';
			cropInfoEl.appendChild(cropTimes);

			var cropX = new Element('span');
			this.cropX = cropInfoEl.appendChild(cropX);
			this.cropX.title = 'Crop X';

			var cropTimes = new Element('span');
			cropTimes.innerHTML = '-';
			cropInfoEl.appendChild(cropTimes);

			var cropY = new Element('span');
			this.cropY = cropInfoEl.appendChild(cropY);
			this.cropY.title = 'Crop Y';

			info.appendChild(cropInfoEl);

			this.info = UI.appendChild(info);

			var buttons = new Element('div',{
				id: 'chaniCropButtons',
				class: 'chaniCropButtons'
			});

			var cancel = new Element('div',{
				id: 'chaniCropButtonCancel',
				class: 'chaniCropButtonLeft'
			});
			cancel.innerHTML = 'cancel';
			cancel.addEvent('click',function() {
				this.removeCropper();
			}.bind(this));

			var save = new Element('div',{
				id: 'chaniCropButtonSave',
				class: 'chaniCropButtonRight'
			});
			save.innerHTML = 'Save';
			save.addEvent('click',function() {
				this.saveCroppedImage();
			}.bind(this));

			buttons.appendChild(cancel);
			buttons.appendChild(save);

			UI.appendChild(buttons);

			wrapper.appendChild(UI);

			var imgWrapper = new Element('div',{
				class: 'chaniImageWrapper'
			});

			imgWrapper.setStyles({
				width: this.settings.cropperWidth,
				height: this.settings.cropperHeight
			});

			this.imgWrapper = wrapper.appendChild(imgWrapper);

			fx = new Fx.Morph(wrapper, {
				duration: 1,
				transition: 'sine:in',
				onStart: function() {
					if(!this.sliderObject) {
						this.sliderObject = new Slider(this.slider, this.knob, {
							wheel: true,
							steps: 100,
							initialStep: this.settings.scale,
							onChange: function(step) {
								if(step < 1) {
									step = 1;
								}
								this.changeImageSize(step);
							}.bind(this)
						});
						this.sliderObject.set(this.settings.scale);
						this.sliderObject.fireEvent('change',this.settings.scale);
					}
				}.bind(this),
				onComplete: function() {
					if(this.removing == true) {
						this.cropper.setStyles({
							width: 0,
							height: 0,
							borderTopWidth: 0,
							borderRightWidth: 0,
							borderBottomWidth: 0,
							borderLeftWidth: 0
						});
						this.imgWrapper.removeChild(this.img);
						this.cropper = document.body.removeChild(this.cropper);
					}
				}.bind(this)
			});
			wrapper.store('fx',fx);

			this.cropper = documentFragment.appendChild(wrapper);
		} else {
			this.step = this.settings.step;
			if(this.backgroundEl) {
				this.backgroundEl = documentFragment.appendChild(this.backgroundEl);
			}
		}

		var img = this.createImageTag();
		this.img = this.imgWrapper.appendChild(img);

		if(!this.cropArea) {
			this.createCropArea();
		}

		this.cropper = documentFragment.appendChild(this.cropper);
		document.body.appendChild(documentFragment);
		this.positionOnTarget();
		this.img.src = this.sUrl;

	},
	changeImageSize: function(step) {
		var newWidth = Math.floor(this.settings.cropperWidth * (step / 100));
		var newHeight = Math.floor(this.settings.cropperHeight * (step / 100));
		var posX = Math.floor((this.settings.cropperWidth - newWidth) / 2);
		var posY = Math.floor((this.settings.cropperHeight - newHeight) / 2);
		this.img.width = newWidth;
		this.img.height = newHeight;

		this.img.setStyles({
			top: posY,
			left: posX
		});

		this.cropArea.measure.setStyles({
			top: posY,
			left: posX,
			width: newWidth,
			height: newHeight,
			backgroundSize: newWidth + 'px, ' + newHeight + 'px'
		});

		var width = Math.ceil(this.naturalWidth * (step / 100));
		var height = Math.ceil(this.naturalHeight * (step / 100));

		this.percentage.innerHTML = step + '%';
		this.imageWidth.innerHTML = width + 'px';
		this.imageHeight.innerHTML = height + 'px';
		this.targetEl.setAttribute('data-scale',step);
		this.step = step;
	},

	updateCropArea: function() {
		var area = this.getArea();
		var clip = 'rect(' + area.y + 'px,'+ area.x2 + 'px,' + area.y2 +'px,' + area.x +'px)';
		this.cropArea.measure.setStyle('clip',clip);
		this.updateCropInfo();
	},
	updateCropInfo: function() {
		var area = this.getRealArea();

		var ratio = this.naturalWidth / this.img.width;

		/*area.x = Math.floor((area.x * ratio) * (this.step / 100));
		 area.y = Math.floor((area.y * ratio) * (this.step / 100));
		 area.width = Math.floor((area.width * ratio) * (this.step / 100));
		 area.height = Math.floor((area.height * ratio) * (this.step / 100));*/

		this.cropX.innerHTML = area.x + 'px';
		this.cropY.innerHTML = area.y + 'px';
		this.cropWidth.innerHTML = area.width + 'px';
		this.cropHeight.innerHTML = area.height + 'px';

		this.targetEl.setAttribute('data-crop-x',area.x);
		this.targetEl.setAttribute('data-crop-y',area.y);
		this.targetEl.setAttribute('data-crop-width',area.width);
		this.targetEl.setAttribute('data-crop-height',area.height);

	},

	getArea: function() {
		var area = {
			x: this.cropArea.area.getStyle('left').toInt(),
			y: this.cropArea.area.getStyle('top').toInt(),
			width: this.cropArea.area.getStyle('width').toInt(),
			height: this.cropArea.area.getStyle('height').toInt()
		};
		area.x2 = area.x + area.width;
		area.y2 = area.y + area.height;
		return area;
	},

	getRealArea: function() {
		var area = this.getArea();
		var ratio = this.naturalWidth / this.img.width;

		area.x = Math.ceil(area.x * ratio);
		area.y = Math.ceil(area.y * ratio);
		area.width = Math.ceil(area.width * ratio);
		area.height = Math.ceil(area.height * ratio);

		return area;
	},

	resetArea: function() {
		//Convert the real crop coordinates to scaled ones
		var ratio = this.img.width / this.naturalWidth;
		var area = {
			x: Math.floor(this.settings.cropX * ratio),
			y: Math.floor(this.settings.cropY * ratio),
			width: Math.ceil(this.settings.cropWidth * ratio),
			height: Math.ceil(this.settings.cropHeight * ratio)
		};
		this.cropArea.area.setStyles({
			left: area.x,
			top: area.y,
			width: area.width,
			height: area.height
		});
		area.x2 = area.x + area.width;
		area.y2 = area.y + area.height;
		var clip = 'rect(' + area.y + 'px,'+ area.x2 + 'px,' + area.y2 +'px,' + area.x +'px)';
		this.cropArea.measure.setStyle('clip',clip);
		this.updateCropInfo();
	},

	moveTop: function(y) {

		var height = this.cropArea.startArea.height;
		var top = this.cropArea.startArea.y + y;

		if(top < 0) {
			height = height + this.cropArea.startArea.y;
			top = 0;
		} else {
			height = height -y;
			height = Math.max(height, 0);
		}

		top = Math.min(top, this.img.height);
		height = Math.min(height, this.img.height);

		return {
			top: top,
			height: height
		}

	},
	moveRight: function(x) {
		var width = this.cropArea.startArea.width - x;

		if(width < 0) {
			width = 0;
		} else {
			width = Math.min(width, (this.img.width - this.cropArea.startArea.x));
		}
		return width;
	},
	moveBottom: function(y) {

		var height = this.cropArea.startArea.height - y;
		if(height < 0) {
			height = 0;
		} else {
			height = Math.min(height, (this.img.height - this.cropArea.startArea.y));
		}

		return height;

	},
	moveLeft: function(x) {
		var width = this.cropArea.startArea.width;
		var left = this.cropArea.startArea.x + x;

		if(left < 0) {
			width = width + this.cropArea.startArea.x;
			left = 0;
		} else {
			width = width - x;
			width = Math.max(width, 0);
		}

		left = Math.min(left, this.img.width);
		width = Math.min(width, this.img.width);


		return {
			left: left,
			width: width
		}
	},

	createCropArea: function() {

		this.cropArea = {
			overlay: new Element('div',{
				class: 'chaniCropImageOverlay'
			}),
			measure: new Element('div',{
				class: 'chaniCropMeasure'
			}),
			area: new Element('div',{
				class: 'chaniCropArea'
			})
		};

		this.cropArea.overlay.setStyles({
			opacity: 0.5,
			top: 0,
			right: 0,
			bottom: 0,
			left: 0
		});

		var center = this.calculateCenter(this.settings.cropperWidth, this.settings.cropperHeight);

		var topHandle= new Element('div',{
			class: 'chaniCropHandle top'
		});
		var topHandleDrag = new Drag(topHandle,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startY = event.page.y;
			}.bind(this),
			onDrag: function(el, event) {

				var y = event.page.y - this.cropArea.startY;

				var values = this.moveTop(y);
				this.cropArea.area.setStyles(values);

				this.updateCropArea();

			}.bind(this)
		});
		topHandle.store('drag',topHandleDrag);

		var topRightHandle = new Element('div',{
			class: 'chaniCropHandle top right'
		});
		var topRightHandleDrag = new Drag(topRightHandle,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startX = event.page.x;
				this.cropArea.startY = event.page.y;
			}.bind(this),
			onDrag: function(el, event) {

				var x = this.cropArea.startX - event.page.x;
				var y = event.page.y - this.cropArea.startY;

				var width = this.moveRight(x);
				var values = this.moveTop(y);

				values.width = width;

				this.cropArea.area.setStyles(values);

				this.updateCropArea();

			}.bind(this)
		});
		topRightHandle.store('drag',topRightHandleDrag);

		var rightHandle = new Element('div',{
			class: 'chaniCropHandle right'
		});
		var rightHandleDrag = new Drag(rightHandle,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startX = event.page.x;
			}.bind(this),
			onDrag: function(el, event) {

				var x = this.cropArea.startX - event.page.x;
				var width = this.moveRight(x);

				this.cropArea.area.setStyle('width',width);
				this.updateCropArea();
			}.bind(this)
		});
		rightHandle.store('drag',rightHandleDrag);

		var bottomRightHandle = new Element('div',{
			class: 'chaniCropHandle bottom right'
		});
		var bottomRightHandleDrag = new Drag(bottomRightHandle,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startX = event.page.x;
				this.cropArea.startY = event.page.y;
			}.bind(this),
			onDrag: function(el, event) {

				var x = this.cropArea.startX - event.page.x;
				var y = (this.cropArea.startY - event.page.y);

				var width = this.moveRight(x);
				var height = this.moveBottom(y);

				this.cropArea.area.setStyles({
					width: width,
					height: height
				});

				this.updateCropArea();

			}.bind(this)
		});
		bottomRightHandle.store('drag',bottomRightHandleDrag);

		var	bottomHandle = new Element('div',{
			class: 'chaniCropHandle bottom'
		});
		var bottomHandleDrag = new Drag(bottomHandle,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startY = event.page.y;
			}.bind(this),
			onDrag: function(el, event) {

				var y = (this.cropArea.startY - event.page.y);
				var height = this.moveBottom(y);

				this.cropArea.area.setStyle('height',height);
				this.updateCropArea();
			}.bind(this)
		});
		bottomHandle.store('drag',bottomHandleDrag);

		var	bottomLeftHandle = new Element('div',{
			class: 'chaniCropHandle bottom left'
		});
		var bottomLeftHandleDrag = new Drag(bottomLeftHandle,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startX = event.page.x;
				this.cropArea.startY = event.page.y;
			}.bind(this),
			onDrag: function(el, event) {

				var x = event.page.x - this.cropArea.startX;
				var y = (this.cropArea.startY - event.page.y);

				var values = this.moveLeft(x);
				var height = this.moveBottom(y);

				values.height = height;

				this.cropArea.area.setStyles(values);

				this.updateCropArea();
			}.bind(this)
		});
		bottomLeftHandle.store('drag',bottomLeftHandleDrag);

		var	leftHandle = new Element('div',{
			class: 'chaniCropHandle left'
		});
		var leftHandleDrag = new Drag(leftHandle,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startX = event.page.x;
			}.bind(this),
			onDrag: function(el, event) {

				var x = event.page.x - this.cropArea.startX;

				var values = this.moveLeft(x);

				this.cropArea.area.setStyles(values);
				this.updateCropArea();
			}.bind(this)
		});
		leftHandle.store('drag',leftHandleDrag);

		var	topLeftHandle = new Element('div',{
			class: 'chaniCropHandle top left'
		});
		var topLeftHandleDrag = new Drag(topLeftHandle,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startX = event.page.x;
				this.cropArea.startY = event.page.y;
			}.bind(this),
			onDrag: function(el, event) {

				var x = event.page.x - this.cropArea.startX;
				var y = event.page.y - this.cropArea.startY;

				var leftValues = this.moveLeft(x);
				var topValues = this.moveTop(y);

				this.cropArea.area.setStyles(leftValues);
				this.cropArea.area.setStyles(topValues);

				this.updateCropArea();

			}.bind(this)
		});
		topLeftHandle.store('drag',topLeftHandleDrag);

		this.imgWrapper.appendChild(this.cropArea.overlay);

		this.cropArea.topHandle = this.cropArea.area.appendChild(topHandle);
		this.cropArea.topLeftHandle = this.cropArea.area.appendChild(topLeftHandle);
		this.cropArea.topRightHandle = this.cropArea.area.appendChild(topRightHandle);

		this.cropArea.leftHandle = this.cropArea.area.appendChild(leftHandle);
		this.cropArea.rightHandle = this.cropArea.area.appendChild(rightHandle);

		this.cropArea.bottomHandle = this.cropArea.area.appendChild(bottomHandle);
		this.cropArea.bottomLeftHandle = this.cropArea.area.appendChild(bottomLeftHandle);
		this.cropArea.bottomRightHandle = this.cropArea.area.appendChild(bottomRightHandle);

		this.cropArea.area = this.cropArea.measure.appendChild(this.cropArea.area);

		var areaDrag = new Drag(this.cropArea.measure,{
			snap: 1,
			preventDefault: true,
			stopPropagation: true,
			compensateScroll: false,
			style: false,
			//invert - (boolean: defaults to false) Whether or not to invert the values reported on start and drag.
			//modifiers - (object: defaults to {'x': 'left', 'y': 'top'}) An object with x and y properties used to indicate the CSS modifiers (i.e. 'left').
			//style - (boolean: defaults to true) Whether or not to set the modifier as a style property of the element.
			//unit - (string: defaults to 'px') A string indicating the CSS unit to append to all number values.
			onStart: function(el, event) {
				this.cropArea.startArea = this.getArea();
				this.cropArea.startX = event.page.x;
				this.cropArea.startY = event.page.y;
			}.bind(this),
			onDrag: function(el, event) {

				var x = event.page.x - this.cropArea.startX;
				var y = event.page.y - this.cropArea.startY;

				var left = this.cropArea.startArea.x + x;
				var top = this.cropArea.startArea.y + y;

				left = Math.max(left, 0);
				left = Math.min(left, this.img.width - this.cropArea.startArea.width);

				top = Math.max(top, 0);
				top = Math.min(top, this.img.height - this.cropArea.startArea.height);

				this.cropArea.area.setStyle('left', left);
				this.cropArea.area.setStyle('top',top);

				this.updateCropArea();

			}.bind(this)
		});

		this.cropArea.measure.store('drag',areaDrag);

		this.imgWrapper.appendChild(this.cropArea.measure);

	},

	createImageTag: function() {
		this.removing = false;
		var img = new Element('img');
		img.addEvent('click',function(event) {
			event.preventDefault();
			event.stopPropagation();
			return false;
		});

		img.setStyles({
			top: 0,
			left: 0
		});

		img.addEvent('load',function(el) {
			this.naturalWidth = el.naturalWidth;
			this.naturalHeight = el.naturalHeight;
			//If the image is smaller than the cropper area, we don't do anything yet
			if(this.naturalWidth < this.settings.cropperWidth || this.naturalHeight < this.settings.cropperHeight) {
				//console.log('settings different values');
				//this.settings.cropperWidth = this.naturalWidth;
				//this.settings.cropperHeight = this.naturalHeight;
				//Square or landscape
				el.width = this.naturalWidth;
				el.height = this.naturalHeight;

				this.settings.cropperWidth = el.width;
				this.settings.cropperHeight = el.height;

				//Otherwise, we make the image fit
			} else {
				if(this.naturalWidth > this.settings.cropperWidth) {
					el.width = this.settings.cropperWidth;
				}

				if(el.height > this.settings.cropperHeight) {
					el.width = '';
					el.height = this.settings.cropperHeight;
				}
			}
			if(this.settings.scale < 100) {
				this.changeImageSize(this.settings.scale);
			}

			var size = el.getDimensions();

			/*if(size.width < this.settings.cropperWidth) {
			 this.settings.width = size.width;
			 this.settings.height = size.height;
			 }

			 if(size.height < this.settings.cropperHeight) {
			 this.settings.width = size.width;
			 this.settings.cropperHeight = size.height;
			 }*/

			var left = Math.floor((this.settings.cropperWidth - size.width) / 2);
			var top = Math.floor((this.settings.cropperHeight - size.height) / 2);

			var data = {
				top: 0,
				left: 0,
				width: this.settings.cropperWidth,
				height: this.settings.cropperHeight
			};
			this.imgWrapper.setStyles(data);

			this.cropArea.measure.setStyles({
				backgroundImage: 'url(' + this.sUrl + ')',
				backgroundSize: el.width + 'px, ' + el.height + 'px'
			});

			if(this.settings.cropWidth > 0 && this.settings.cropHeight > 0) {
				this.resetArea();
			} else {
				this.cropArea.measure.setStyles({
					width: this.settings.cropperWidth,
					height: this.settings.cropperHeight
				});
				this.cropArea.area.setStyles({
					top: 0,
					left: 0,
					width: this.settings.cropperWidth,
					height: this.settings.cropperHeight
				});
			}

			this.updateCropArea();

			this.topLeft = this.calculateTopLeft();

			var infoFx = this.info.retrieve('fx');
			var cropperFx = this.cropper.retrieve('fx');
			if(this.options.backgroundOverlay == true) {
				this.animateBackground();
			}
			cropperFx.start({
				'top': [this.targetPosition.top, this.topLeft.top],
				'left': [this.targetPosition.left, this.topLeft.left],
				'width': [this.targetDimensions.width, 640],//this.settings.cropperWidth],
				'height': [this.targetDimensions.height, this.settings.cropperHeight],//this.settings.cropperHeight],
				'borderTopWidth': [0, this.settings.borderTopWidth],
				'borderLeftWidth': [0, this.settings.borderLeftWidth],
				'borderBottomWidth': [0, this.settings.borderBottomWidth],
				'borderRightWidth': [0, this.settings.borderRightWidth]
			});
			infoFx.start(1);
		}.bind(this, img));

		var fx = new Fx.Tween(img,{
			property: 'opacity',
			duration: 900
		});
		img.store('fx',fx);
		return img;
	},
	animateBackground: function() {
		var windowWidth= window.innerWidth;
		var windowHeight = window.innerHeight;

		var bgFx = this.backgroundEl.retrieve('fx');
		bgFx.start({
			'opacity': [0, 0.3],
			'top': [this.targetPositionBody.y, 0],
			'left': [this.targetPositionBody.x, 0],
			'width': [this.targetDimensions.width, Math.max(document.body.scrollWidth, windowWidth)],
			'height': [this.targetDimensions.height, Math.max(document.body.scrollHeight, windowHeight)]
		});
	},

	calculateCenter: function(width, height) {

		var centerX = Math.floor(width / 2);
		var centerY = Math.floor(height / 2);

		return {x: centerX, y: centerY};
	},
	calculateTopLeft: function() {
		var windowWidth = window.innerWidth;
		var windowHeight = window.innerHeight;

		var center = this.calculateCenter(windowWidth, windowHeight);

		var left = center.x - (Math.ceil(this.settings.cropperWidth / 2) + this.settings.borderLeftWidth + this.settings.borderRightWidth);
		var top = center.y - (Math.ceil(this.settings.cropperHeight / 2) + this.settings.borderTopWidth + this.settings.borderBottomWidth);

		return {left: left, top: top};
	},

	positionOnTarget: function() {
		this.cropper.setStyles({
			'top': this.targetPosition.top,
			'left': this.targetPosition.left,
			'width': 0,
			'height': 0
		});
	},
	removeCropper: function() {
		this.removing = true;
		var cropperFx = this.cropper.retrieve('fx');
		var infoFx = this.info.retrieve('fx');

		if(this.options.backgroundOverlay == true) {
			var windowWidth= window.innerWidth;
			var windowHeight = window.innerHeight;

			var bgFx = this.backgroundEl.retrieve('fx');
			bgFx.start({
				'opacity': [0.3, 0],
				'top': [0, this.targetPositionBody.y],
				'left': [0, this.targetPositionBody.x],
				'width': [Math.max(document.body.scrollWidth, windowWidth), this.targetDimensions.width],
				'height': [Math.max(document.body.scrollHeight, windowHeight), this.targetDimensions.height]
			});
		}
		infoFx.start(0);
		cropperFx.start({
			'top': [this.topLeft.top, this.targetPosition.top],
			'left': [this.topLeft.left, this.targetPosition.left],
			'width': [this.settings.cropperWidth, this.targetDimensions.width],
			'height': [this.settings.cropperHeight, this.targetDimensions.height],
			'borderTopWidth': [this.settings.borderTopWidth, 0],
			'borderLeftWidth': [this.settings.borderLeftWidth, 0],
			'borderBottomWidth': [this.settings.borderBottomWidth, 0],
			'borderRightWidth': [this.settings.borderRightWidth,0]
		});
		console.log('removed cropper');
	},
	removeOverlay: function() {
		this.backgroundEl = document.body.removeChild(this.backgroundEl);
	},

	saveCroppedImage: function() {
		var area = this.getRealArea();

		var data = {
			iMediaId: this.iMediaId,
			scale: this.step,
			imageWidth: this.img.width,
			imageHeight: this.img.height,
			cropX: area.x,
			cropWidth: area.width,
			cropY: area.y,
			cropHeight: area.height
		};

		var req = new Request({
			url: this.options.saveMediaUrl,
			data: data,
			onSuccess: function(txt) {
				if(this.options.demo == false) {
					this.targetEl.src = txt+'&random='+Math.random();
				} else {
					var aResult = JSON.decode(txt);
					this.targetEl.src = aResult['thumb']+'&random='+Math.random();
					var el = $('chaniImageCropperDemoCrop');
					var img = document.createElement('img');
					img.src = aResult['crop']+'&random='+Math.random();
					while(el.lastChild) {
						el.removeChild(el.lastChild);
					}
					el.appendChild(img);

				}
				this.removeCropper();
			}.bind(this)
		}).send();
	},

	removeDemoCrop: function() {
		var data = {
			iMediaId: this.iMediaId
		};
		var req = new Request({
			url: this.options.removeCropUrl,
			data: data,
			onSuccess: function(txt) {
				alert(txt);
			}.bind(this)
		}).send();
	},
	resetCrop: function() {
		var data = {
			iMediaId: this.iMediaId
		};
		var req = new Request({
			url: this.options.resetCropUrl,
			data: data,
			onSuccess: function(txt) {
				alert(txt);
			}.bind(this)
		}).send();
	}
});

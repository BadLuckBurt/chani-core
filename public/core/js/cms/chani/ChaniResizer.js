//Resize functionality for spice blocks
//TODO: Move to Spice.js or keep separate?
var ChaniResizer = new Class({
	Implements: Options,
	options: {
		selector: '.resize-handles',
		labelSelector: '.block-width',
		autoHeight: true,
		containerSelector: 'blocks',
		columns: 5
	},
	initialize: function(options) {
		this.setOptions(options);
		this.container = $(this.options.containerSelector);;

		if(this.container) {
			this.fullWidth = this.container.getStyle('width').toInt();
			this.percentage = Math.ceil(this.fullWidth / 100);  //Number of pixels the user will need to move for 1 percent increase / decrease of width
			this.newX = 0;  //Used to calculate the number of pixels the users has moved
			this.minWidth = Math.floor(100 / this.options.columns);    //Minimum width in percentages
			this.minHeight = 50;    //Meh, doesn't do anything atm
			this.targets = [];  //Stores the resize targets
			this.handles = [];  //Stores the resize handles
			this.labels = [];   //Stores the labels that show the current width
			this.maxWidth = []; //Pretty much always 100 atm, may be obsolete - could be done by block maybe for fixed template behaviour
			this.currentIndex = -1; //Current index of the above arrays to be targeted upon resizing

			this.mouseMoveBind = this.mouseMove.bind(this); //Used to handle mouse move event during resize
			this.mouseUpBind = this.mouseUp.bind(this); //Used to handle mouse up event during resize

			this.assignEvents(this.options.selector);   //Magic happens
		}
	},
	assignEvents: function(selector) {
		var self = this;
		var els = $$(selector); //Load the elements
		var indexLength = this.targets.length;  //Used to calculate the correct index in the each loop below
		for(var i = 0; i < els.length; i++) {
			this.maxWidth[this.maxWidth.length] = this.fullWidth; //Maybe be obsolete
			this.targets[this.targets.length] = $(els[i].parentNode.parentNode); //The element that will be resized
			//this.targets[this.targets.length] = $(els[i].getAttribute('data-target')); //The element that will be resized
			var handles = els[i].getElements('i');    //Get the handle(s)
			handles.each(function(item, index) {
				//Get the element's label to update the displayed width upon resize
				var label = item.parentNode.getElement(self.options.labelSelector);
				label.addEvent('click', function (index, event) {
					this.currentIndex = index;
					var width = this.targets[this.currentIndex].getStyle('width').replace('%', '');
					var result = prompt('Please enter a width percentage between 20 and 100', width);
					if(result !== null) {
						width = parseInt(result, 10);
						this.targets[this.currentIndex].setStyle('width', width + '%');
						this.labels[this.currentIndex].innerHTML = width + '%';
						this.saveValue();
					}
					event.stopPropagation();
					event.preventDefault();
				}.bind(self, i + indexLength));
				//Get the element's label to update the displayed width upon resize
				self.labels[self.labels.length] = label;
				item.addEvents({
					mousedown: function(index, event) {
						//Record the x and y position to later calculate how far the user moved his mouse
						this.x = event.client.x;
						this.y = event.client.y;

						//Store the element being used as drag handle
						this.dragHandle = event.target;
						//Store the index so we know which elements to reference
						this.currentIndex = index;
						//Add class to show stuff is habbening
						this.targets[this.currentIndex].addClass('resizing');

						//Add events to handle user actions
						window.addEvent('mousemove',this.mouseMoveBind);
						window.addEvent('mouseup', this.mouseUpBind);
						//Reset newX to avoid jumpy width setting through leftover value
						this.newX = 0;
						event.preventDefault();
					}.bind(self, i + indexLength)
				});
			});
		}
	},
	mouseMove: function(event) {
		var offsetX = event.client.x - this.x;
		this.newX += offsetX;
		//Check wether the user has moved enough to increase / decrease width of the target element
		if(this.newX >= this.percentage || this.newX <= this.percentage * -1) {
			//We're gonna try to resize, yay
			var percentage = Math.floor(this.newX / this.percentage);
			//If there's no auto height, the user's dragging will influence height and width at the same time
			if(this.options.autoHeight === false) {
				var height = this.targets[this.currentIndex].getStyle('height').toInt();
				var offsetY = event.client.y - this.y;
				height = height + offsetY;
				height = Math.max(height, this.minHeight);
				this.targets[this.currentIndex].setStyle('height', height + 'px');
			}
			//Store the new start position for future movement calculations
			this.x = event.client.x;
			this.y = event.client.y;
			//Get the current width (in percentages) and add the percentage
			var width = this.targets[this.currentIndex].getStyle('width').toInt();
			width += percentage;
			//Clip width to min - max values
			width = Math.min(width, 100);
			width = Math.max(width, this.minWidth);
			//Set target element's width and update label text
			this.targets[this.currentIndex].setStyle('width',width + '%');
			this.labels[this.currentIndex].innerHTML = width + '%';
			//Reset newX or resizing will get faster and faster
			this.newX = 0;
			return true;
		} else {
			return false;
		}
	},
	mouseUp: function() {
		this.saveValue();
	},
	saveValue: function() {
		var saveValue = new Request({
			url: this.targets[this.currentIndex].getAttribute('data-url'),
			data: {
				id: this.targets[this.currentIndex].getAttribute('data-id'),
				sColumn: this.targets[this.currentIndex].getAttribute('data-column'),
				sValue: this.targets[this.currentIndex].getStyle('width').toInt(),
				bLocale: this.targets[this.currentIndex].getAttribute('data-locale'),
				iLocaleId: 116
			}
		}).send();
		this.reset();
	},
	reset: function() {
		this.dragHandle = null;
		//Remove resize indicators and window events
		this.targets[this.currentIndex].removeClass('resizing');
		window.removeEvent('mousemove', this.mouseMoveBind);
		window.removeEvent('mouseup', this.mouseUpBind);
		//Reset index and base coordinates
		this.currentIndex = -1;
		this.x = 0;
		this.y = 0;
	}
});
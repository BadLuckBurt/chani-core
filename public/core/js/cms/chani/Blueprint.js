//Global variable to track all objects necessary for saving module values
var oChaniObjects = {};

//Switches language for all modules
function switchLanguage(iLocaleId) {
	oChaniObjects.each(function(item, index) {
		item.changeLanguage(iLocaleId);
	});
}

//Blueprint class to be extended by Module classes
var chaniBlueprintCms = new Class ({
	Implements: Options,
	options: {
		id: 0,
		iLocaleId: 0,
		ajax: {
			saveValueUrl: '',
			addSpiceUrl: '',
			previewUrl: '',
			cancelUrl: '',
			resetUrl: ''
		},
		cancelMessage: '',
		className: 'blueprint',
		master: false
	},
	iLocaleId: 116,
	initialize: function(options) {
		//Set class options and attributes
		this.setOptions(options);
		this.iLocaleId = this.options.iLocaleId;
		this.form = $('dashboard-content-toolbar');
		if(this.form) {
			//Retrieve all form field related to the module (determined by options.className)
			this.fields = this.form.getElements('input.' + this.options.className + ', select.' + this.options.className + ', textarea.' + this.options.className);

			//Assign all field events (defined by data-attributes)
			for (var i = 0; i < this.fields.length; i++) {
				this.assignEventsToEl(this.fields[i]);
			}
		}
		//Store reference in ChaniObjects
		oChaniObjects[this.options.className] = this;
		//TODO: What was this meant for again?
		if(this.options.master == true) {

		}
	},
	getId: function(oEl) {
		//Retrieve the record id from a formfield
		var id = this.options.id;
		if(id == 0) {
			id = oEl.getAttribute('data-id');
		}
		return id;
	},
	saveValue: function(oEl) {
		//Save field value through AJAX
		var id = oEl.getAttribute('data-id');
		var sColumn = oEl.getAttribute('data-column');
		var sValue;
		/*if(sColumn == 'iWidthPercentage') {
			sValue = oEl.getAttribute('data-value');
			if(sValue == '') {
				sValue = oEl.value;
			}
		} else {*/
			sValue = oEl.value;
		/*}*/
		var bLocale = oEl.getAttribute('data-locale');

		var req = new Request({
			url: this.options.ajax.saveValueUrl,
			data: {
				id: id,
				sValue: sValue,
				sColumn: sColumn,
				bLocale: bLocale,
				iLocaleId: this.iLocaleId
			},
			onSuccess: function(txt) {

				if(txt == 'success') {
					var fx = this.retrieve('fx', false);
					if(fx !== false) {
						fx.start('#4caf50');
						fx.start.delay(1500,fx,'#111');
					}
				} else {
					alert('error');
				}
			}.bind(oEl)
		}).send();
	},
	toggleValue: function(oEl) {
		//Save a toggle value through AJAX
		var sValue = '0';
		if(oEl.checked === true) {
			sValue = '1';
		}
		var sColumn = oEl.name;
		var bLocale = oEl.getAttribute('data-locale');

		var id = this.getId(oEl);
		var req = new Request({
			url: this.options.ajax.saveValueUrl,
			data: {
				id: id,
				sValue: sValue,
				sColumn: sColumn,
				bLocale: bLocale,
				iLocaleId: this.iLocaleId
			},
			onSuccess: function(txt) {

				if(txt == 'success') {
					var fx = this.retrieve('fx');
					if(fx) {
						fx.start('#AEA');
						fx.start.delay(1500, fx, '#FFF');
					}
				} else {
					alert('error');
				}

			}.bind(oEl)
		}).send();
	},
	assignEventsToEl: function(el) {
		//Assign defined events to a formfield
		var events = el.getAttribute('data-events');
		if(events !== null) {
			events = events.split('|');
			for(var j = 0; j < events.length; j++) {
				var data = el.getAttribute('data-' + events[j]);
				if(data === null) {
					alert('Error for field \'' + events[j] + '\'. Function and or parameters not given.');
				} else {
					data = data.split('|');
					this.handleEvent(el, events[j], data);
				}
			}
		}
	},
	handleEvent: function(el, event, params) {
		//Handles the assignment of each event to a field
		//TODO: Change background-color fx to CSS3 instead of Fx.Tween
		var fxSuccess = null;
		switch (params[0]) {
			case 'saveValue':
				el.addEvent(event, function(el) {
					this.saveValue(el);
				}.bind(this, el));

				fxSuccess = new Fx.Tween(el, {
					property: 'background-color',
					duration: 1000,
					onComplete: function() {
						if(this.element.getStyle('background-color') == '#111111') {
							this.element.setStyle('background-color','');
						}
					}
				});
				el.store('fx', fxSuccess);
				break;
			case 'toggleValue':
				el.addEvent(event, function(el) {
					this.toggleValue(el);
				}.bind(this,el));

				fxSuccess = new Fx.Tween(el, {
					property: 'background-color',
					duration: 1000
				});
				el.store('fx', fxSuccess);
				break;
			default:
				alert('function not found: ' + params[0]);
				break;
		}
	},
	changeLanguage: function(iLocaleId) {
		//Update the language id according to the users choice and refresh the appropriate fields
		this.iLocaleId = iLocaleId;
		var aLocaleValues = this.getLocaleValues();
	},
	//TODO: Make this function operational
	getLocaleValues: function() {
		//Retrieve the localized form values for this model
		return [];
	},
	//TODO: Check this function
	saveEditor: function(oEl, sValue) {
		//Save CKEditor value through AJAX and set changed text as field's content
		var sModel = oEl.getAttribute('data-model').toLowerCase();
		var sColumn = oEl.getAttribute('data-column');
		var bLocale = oEl.getAttribute('data-locale');
		var id = oEl.getAttribute('data-id');

		var req = new Request({
			url: '/cms/'+this.options.className+'/saveValue',
			data: {
				id: id,
				sValue: sValue,
				sColumn: sColumn,
				sModel: sModel,
				bLocale: bLocale,
				iLocaleId: this.iLocaleId
			},
			onSuccess: function(txt) {
				if(txt == 'success') {
					var fx = this.retrieve('fx');
					fx.start('#AEA');
					fx.start.delay(1500,fx,'#FFF');
				} else {
					alert('error');
				}
			}.bind(oEl)
		}).send();
	},
	//TODO: WHERE IS THIS FUNCTION USED?
	toggleSubPages: function(el) {
		var ul = el.parentNode.parentNode.parentNode;
		var isUl = false;
		if(ul.hasAttribute('data-toggle')) {
			isUl = true;
			ul.setStyle('height','');
		}
		var toggle = $(el.getAttribute('data-target'));

		if(toggle.getAttribute('data-toggle') == 'closed') {
			toggle.setAttribute('data-toggle','open');
			toggle.setStyle('height',toggle.getAttribute('data-height') + 'px');
		} else {
			//var height = toggle.getSize().y;
			//toggle.setAttribute('data-height',height + 'px');
			toggle.setStyle('height','0px');
			toggle.setAttribute('data-toggle','closed');
		}
		if(isUl) {
			setTimeout(function () {
				var height = ul.getSize().y;
				ul.setStyle('height', height);
				ul.setAttribute('data-height', height);
			}, 502);
		}
		return true;
	},

	addSpice: function(targetId) {
		var data = {
			iModelId: this.options.id,
			sModel: this.options.className,
			iTargetId: target
		};
		console.log(data);
		return;
		var req  = new Request({
			url: this.options.ajax.addSpiceUrl,
			data: data,
			onComplete: function(txt) {
				var documentFragment = new DocumentFragment();
				var aResult = JSON.decode(txt);
				var divWrapper = new Element('div',{
					class: 'spiceWrapper'
				});
				var div = new Element('div',{
					id: 'spice_' + aResult[0],
					class: 'spice sandLightBG'
				});
				div.setAttribute('data-id',aResult[0]);
				div.innerHTML = aResult[1][0];
				div = divWrapper.appendChild(div);
				documentFragment.appendChild(divWrapper);
				$('spices').appendChild(documentFragment);
				oChaniObjects['spice'].assignEvents(div);

				var aSliders = $$('#' + div.id + ' .sliderWrapper');
				for(var i = 0; i < aSliders.length; i++) {
					oChaniObjects['spice'].assignSlider(aSliders[i]);
				}
			}.bind(this)
		}).send();
	}
});
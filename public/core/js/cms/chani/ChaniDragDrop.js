//Drag and drop functionality for spice content icons
//TODO: Applicable for spice blocks too?
//TODO: Redo comments in this file
var ChaniDragDrop = new Class({
	Implements: Options,
	options: {
		draggablesSelector: '.draggables',
		draggableSelector: '.draggable',
		droppableSelector: '.block',
		parentDroppableSelector: 'blocks'
	},
	targetId: 0,
	initialize: function(options) {
		var self = this;
		self.setOptions(options);
		//Set and store bound functions for this class so they can be added to / removed from elements easily.
		self.createBinds();

		//Get the draggables container element(s) and prepare to store the various draggables and targets
		self.draggables = $$(this.options.draggablesSelector);
		self.draggableItems = [];
		self.targets = [];
		self.draggables.each(function(container, index) {
			//Store target element for future reference
			self.targets[index] = $(container.getAttribute('data-target'));
			//Loop through the draggable items of this container and set up their events
			var draggableItems = container.getElements(self.options.draggableSelector);
			draggableItems.each(function(draggable, i) {
				draggable.addEvent('mousedown',function(el, index, event) {
					//Store the currently dragged element and clone it
					this.dragged = el;
					this.clone = this.dragged.clone(true, false);
					this.clone.addClass('clone');
					this.clone.setStyles({
						top: (event.client.y + window.scrollY) + 10,
						left: event.client.x + 10
					});
					document.body.appendChild(this.clone);
					//Set the current droppable target
					this.target = this.targets[index];
					//Add event listeners for dragging / mouse release
					this.startDrag();
					event.preventDefault();
				}.bind(self, draggable, index));
			});
			self.draggableItems = draggableItems;
		});
		//TODO: Make this more flexible
		$$('.addspice-cancel, .addspice-save').each(function(item,index) {
			if (item.hasClass('save')) {
				item.addEvent('click', function() {
					var iTargetId = $('spiceTargetId').value;
					var sType = $('spiceType').value;
					var sPosition = $('spicePosition').value;
					oChaniObjects['spice'].add(iTargetId, sType, sPosition);
				}.bind(self));
				return true;
			}
			if (item.hasClass('cancel')) {
				item.addEvent('click', function(el) {
					$('spiceTargetId').value = 0;
					Chani.toggleModalContent(el, false, false);
					//this.toggleEditor(false,'');
				}.bind(self, item));
				return true;
			}
		});
		$$('.media-cancel, .media-save').each(function(item,index) {
			if (item.hasClass('save')) {
				item.addEvent('click', function() {
					console.log('clicked Add button');
					//this.addSpice();
				}.bind(self));
				return true;
			}
			if (item.hasClass('cancel')) {
				item.addEvent('click', function(el) {
					Chani.toggleModalContent(el, false, false);
					//this.toggleEditor(false,'');
				}.bind(self, item));
				return true;
			}
		});
		$$('.spice-cancel, .spice-save').each(function(item,index) {
			if (item.hasClass('save')) {
				item.addEvent('click', function(el) {
					Chani.toggleModalContent(el, false, false);
				}.bind(self, item));
				return true;
			}
			if (item.hasClass('cancel')) {
				item.addEvent('click', function(el) {
					Chani.toggleModalContent(el, false, false);
				}.bind(self, item));
				return true;
			}
		});

	},
	createBinds: function() {
		var self = this;
		self.startDragBind = self.startDrag.bind(this);
		self.mouseMoveBind = self.mouseMove.bind(this);
		self.mouseUpBind = self.mouseUp.bind(this);
		self.mouseEnterBind = self.mouseEnter.bind(this);
		self.mouseLeaveBind = self.mouseLeave.bind(this);
		self.stopDragBind = self.stopDrag.bind(this);
	},
	startDrag: function() {
		this.targetId = 0;
		document.body.addClass('dragging');
		window.addEvents({
			mousemove: this.mouseMoveBind,
			mouseup: this.mouseUpBind
		});
		this.target.addEventListener('mouseenter',this.mouseEnterBind, true);
	},
	mouseMove: function(event) {
		//Offset the clone slightly or it would be directly on the mouse, triggering annoying events
		var values = {
			top: (event.client.y + window.scrollY) + 10,
			left: event.client.x + 10
		};
		this.clone.setStyles(values);
	},
	mouseEnter: function(event) {
		//Check what element we're entering
		var el = $(event.target);
		if(el !== null && el !== undefined) {
			//Courtesy of Mozilla, why do I care when I enter a text node?
			if (el.tagName === undefined) {
				el = el.getParent('.' + this.options.droppableSelector);
			}
			//Hopefully that if statement above actually got us an element :O
			if (el.tagName !== undefined) {
				console.log(this.options.parentDroppableSelector);
				//We've entered an element that we can drop the clone on
				if (el.hasClass(this.options.droppableSelector) ||
					el.hasClass(this.options.parentDroppableSelector)) {
					this.droppable = el;
					this.droppable.addEvent('mouseleave', this.mouseLeaveBind);
					event.stopPropagation();
					event.preventDefault();
				}
			}
		}
	},
	mouseLeave: function(event) {
		//I guess it's sad to see us leave
		if(this.droppable) {
			//It's cancelling the event it was holding in our honour :(
			this.droppable.removeEvent('mouseleave',this.mouseLeaveBind);
			//Sometimes when leaving one block, we enter another
			if(event.event.relatedTarget.hasClass(this.options.droppableSelector)) {
				//This block is happy to see us arrive and adds an event to itself :D
				this.droppable = event.event.relatedTarget;
				this.droppable.addEvent('mouseleave',this.mouseLeaveBind);
			} else {
				//It's pretty definitive now, we're not dropping the clone on that element
				this.droppable = null;
			}
		}
		event.stopPropagation();
		event.preventDefault();
	},
	mouseUp: function() {
		//Some people could use this function too
		this.stopDrag();
	},
	stopDrag: function() {
		//Ok, we're dropping this clone on a target (target is always a block-overlay element)
		if(this.droppable !== null && this.droppable !== undefined) {
			if(this.droppable.hasClass(this.options.parentDroppableSelector)) {
				$('spiceTargetId').value = 0;
			} else {
				//The id of the block the clone was dropped on
				$('spiceTargetId').value = this.droppable.parentNode.getAttribute('data-id');
			}
			//The type of content the user was dragging
			var type = this.clone.getAttribute('data-type');
			//Select type in dropdown list, user can change this if necessary
			var spiceTypeOptions = $$('#spiceType option');
			for(var i = 0; i < spiceTypeOptions.length; i++) {
				if(spiceTypeOptions[i].value == type) {
					spiceTypeOptions[i].selected = true;
				} else {
					spiceTypeOptions[i].selected = false;
				}
			}
			//TODO: Create inputs for all spice block types: text, media, video and forms
			Chani.toggleModalContent(this.clone, true, false);
			//TODO: Create AJAX call to add a spice block
			if(2 == 3) {
				//Let's prepare the new element
				var el = $(document.createElement('div'));
				el.id = 'block_' + (this.target.children.length + 1);
				el.addClass('block');
				var innerHTML = this.droppable.parentNode.innerHTML;
				el.innerHTML = innerHTML;

				//Change the target's width so the new element can sit next to it
				this.droppable.parentNode.setStyle('width', '50%');
				el.setStyle('width', '50%');
				//Can we leave it to the parent to find a spot or do we need to squeeze this new addition in ourselves?
				var sibling = this.droppable.parentNode.nextElementSibling;
				if (sibling === null) {
					el = this.target.appendChild(el);
				} else {
					//My, grandma...what big teeth you have
					el = this.droppable.parentNode.parentNode.insertBefore(el, sibling);
				}
				//Ofcourse this new element is going to be resizable
				Chani.resizer.assignEvents('#' + el.id + ' .resize-handles');
				this.droppable = null;
			}
		}
		//Remove the event on the target and clear the dragged and target references
		this.target.removeEventListener('mouseenter',this.mouseEnterBind, true);
		//Kill them, kill them all!
		this.clone = document.body.removeChild(this.clone);
		this.clone.destroy();
		//Remove the events we tied to the window
		window.removeEvents({
			mousemove: this.mouseMoveBind,
			mouseup: this.mouseUpBind
		});
		this.dragged = null;
		this.target = null;
		document.body.removeClass('dragging');
	}
});
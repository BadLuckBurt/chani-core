//Class to handle dashboard behaviour
//Contains the functionality for navigation, toolbar and modal windows
//As well as resizing of spice blocks
//TODO: Move resizing code to Spice.js?
var ChaniDashboard = new Class({
	Implements: Options,
	options: {
		navId: 'dashboard-nav',
		toolbarId: 'dashboard-content-toolbar',
		settingsWrapperId: 'dashboard-content-settings-wrapper',
		settingsButtonId: 'pageSettings',
		overlayId: 'dashboard-overlay',
		mainNavClass: '.mainNav',
		dragOptions: {
			draggablesSelector: '.dashboard-content-toolbar',
			draggableSelector: '.buttons-left a',
			droppableSelector: 'block-overlay'
		},
		resizerOptions: {
			selector: '.resize-handles',
			autoHeight: true,
			containerSelector: 'page-blocks',
			columns: 5
		}
	},
	editor: null,
	deleteTarget: null,
	initialize: function(options) {
		var self = this;
		this.setOptions(options);
		//contentTarget and modalTarget are used to reference active content and modal overlays
		this.contentTarget = null;
		this.modalTarget = null;
		this.editorTarget = null;
		this.imageTarget = null;
		this.videoTarget = null;
		this.formTarget = null;
		this.overlay = $(this.options.overlayId);

		//The toolbar contains the content types that can be added to whatever you're editing
		this.toolbar = $(this.options.toolbarId);
		if(this.toolbar) {
			//Store the vertical position of the toolbar to check during the scroll event
			//this.toolbar.setAttribute('data-pos-y', this.toolbar.getPosition(document.body).y);
			//Monitor window scrolling and change position type of toolbar when necessary so it's always fixed
			//at the top of the screen
			window.addEvent('scroll', function () {

				if(window.scrollY > 128) {
					this.settings.setStyle('height', '0');
					this.settings.setAttribute('data-collapsed', 'true');
				} else if(window.scrollY == 0) {
					this.settings.setStyle('height', this.settings.getAttribute('data-height') + 'px');
					this.settings.setAttribute('data-collapsed', 'false');
				}

			}.bind(self));

		}

		//The settings hold global content settings that can be edited (page title etc.)
		this.settings = $(this.options.settingsWrapperId);
		if(this.settings) {
			var height = this.settings.getSize().y
			this.settings.setAttribute('data-height', height);
			this.settings.setStyle('height',height + 'px');
			//this.settings.setStyle('height', '0');
			//this.settings.setAttribute('data-collapsed', 'true');
			//The button to open the settings element
			this.settingsBtn = $(this.options.settingsButtonId);
			if(this.settingsBtn) {
				//Toggles the settings element open or closed
				this.settingsBtn.addEvent('click', function (event) {
					if (this.settings.getAttribute('data-collapsed') == 'true') {
						this.settings.setStyle('height', this.settings.getAttribute('data-height') + 'px');
						this.settings.setAttribute('data-collapsed', 'false');
					} else {
						this.settings.setStyle('height', '0');
						this.settings.setAttribute('data-collapsed', 'true');
					}
					event.preventDefault();
					event.stopPropagation();
					return false;
				}.bind(self));
			}
		}
		//The element that holds the CMS navigation / functionality
		this.wrapper = $(this.options.navId);
		//Collapse or reveal CMS navigation when clicking
		this.wrapper.addEvent('click',function(event) {
			return this.toggleNavElement(event);
		}.bind(self));

		//All links that give access to sub navigation
		var mainNavs = this.wrapper.getElements(this.options.mainNavClass);
		mainNavs.each(function(item,index) {

			if(item.hasClass('logout')) {
				item.addEvent('click', function(el, event) {
					event.stopPropagation();
					return true;
				}.bind(self, item));
			} else {
				//The ul with sub navigation will collapse or reveal itself, depending on it's state.
				item.addEvent('click', function (el, event) {
					return this.toggleNav(el, event);
				}.bind(self, item));
			}
		});
		//Measure and store the height of nested uls then collapse them
		this.prepareNav();

		var subNavs = this.wrapper.getElements('li li a');
		subNavs.each(function(item,index) {

			if(item.hasClass('active')) {
				item.addEvent('click', function (el, event) {
					return this.toggleCMSContent(el, event);
				}.bind(self, item));
			} else {
				item.addEvent('click', function (el, event) {
					event.stopPropagation();
					return true;
				}.bind(self, item));
			}
		});

		this.drag = new ChaniDragDrop(this.options.dragOptions);
		this.resizer = new ChaniResizer(this.options.resizerOptions);

		oChaniObjects['cropper'] = new chaniCrop();

		//Initialize editor
		$$('.editor').each(function(item,index) {
			this.editor = CKEDITOR.replace(item);
		}.bind(self));

		$$('.editor-cancel, .editor-save').each(function(item,index) {
			if (item.hasClass('save')) {
				item.addEvent('click', function() {
					this.saveEditor();
				}.bind(self));
				return true;
			}
			if (item.hasClass('cancel')) {
				item.addEvent('click', function() {
					this.toggleEditor(false,'');
				}.bind(self));
				return true;
			}
		});

		$$('.image-cancel, .image-save').each(function(item,index) {
			if (item.hasClass('save')) {
				item.addEvent('click', function(el) {
					//oChaniObjects['media'].saveMediaValues();
					Chani.toggleModalContent(el, false, false);
				}.bind(self, item));
				return true;
			}
			if (item.hasClass('cancel')) {
				item.addEvent('click', function(el) {
					Chani.toggleModalContent(el, false, false);
				}.bind(self, item));
				return true;
			}
		});
	},
	prepareNav: function() {
		var nestedUls = this.wrapper.getElements('li ul');
		//Measure and store the height of nested uls then collapse them
		nestedUls.each(function(item,index) {
			item.removeClass('collapse-nav-item');
			item.setAttribute('data-height',item.getSize().y);
			var el = $(item.id.replace('-sub',''));
			if(!el.hasClass('active')) {
				item.addClass('collapse-nav-item');
			}
		});
	},
	toggleNav: function(el, event) {
		//Get the sub navigation id from the clicked navigation link
		var nav = $(el.id + '-sub');
		//Reveal sub navigation
		if(nav.hasClass('collapse-nav-item')) {
			nav.setStyle('height',nav.getAttribute('data-height') + 'px');
			nav.removeClass('collapse-nav-item');

			this.dashboardScroll.delay(510,this,true);
		//Hide sub navigation
		} else {
			nav.addClass('collapse-nav-item');
			nav.setStyle('height','');
			this.dashboardScroll.delay(510,this,false);
		}
		event.stopPropagation();
		event.preventDefault();
		return false;
	},
	dashboardScroll: function(scroll) {
		//scrollbar gets added when wrapper content overflows
		if(this.wrapper.scrollHeight <= window.innerHeight) {
			this.wrapper.removeClass('scroll');
		} else if(!this.wrapper.hasClass('scroll')) {
			this.wrapper.addClass('scroll');
		}
	},
	toggleNavElement: function(event) {
		//Collapse or reveal dashboard navigation
		var parent = this.wrapper.parentNode;
		if (parent.hasClass('collapse-nav')) {
			parent.removeClass('collapse-nav');
		} else {
			parent.addClass('collapse-nav');
		}
		event.stopPropagation();
		event.preventDefault();
		return false;
	},

	toggleModalContent: function (el, open, ignore, event) {
		//Reset dashboard content, ignore will be true when opening a modal window related to dashboard content
		if(this.contentTarget !== null && ignore === false) {
			this.contentTarget.addClass('collapse');
			this.contentTarget = null;
		}
		//Hide any current modal content if ids do not match
		var content = $('dashboard-modal-' + el.getAttribute('data-target'));
		if(this.modalTarget !== null && this.modalTarget.id !== content.id) {
			this.modalTarget.setStyle('display','none');
		}
		//Open modal content and save reference
		if(open) {
			this.overlay.setStyle('display','block');
			content.setStyle('display','block');
			this.modalTarget = content;
			var target = el.getAttribute('data-target');
			var type = el.getAttribute('data-type');
			if(target == 'spice') {
				oChaniObjects['spice'].id = el.getAttribute('data-id');
				oChaniObjects['spice'].assignValues();
				if(type == 'image' || type == 'video') {
					$('mediaForm').style.display = 'block';
					html5upload.model = el.getAttribute('data-model');
					html5upload.id = el.getAttribute('data-id');
					html5upload.url = el.getAttribute('data-url');
					html5upload.type = el.getAttribute('data-type');
					html5upload.toggleForm();
				} else {
					$('mediaForm').style.display = 'none';
				}
			} else if(target == 'video' || target == 'image') {
				oChaniObjects['media'].id = el.getAttribute('data-id');
				oChaniObjects['media'].assignValues();
			}
		//Close modal content and clear reference
		} else {
			this.overlay.setStyle('display','none');
			content.setStyle('display','none');
			this.modalTarget = null;
		}
		if(event) {
			event.stopPropagation();
			event.preventDefault();
		}
	},
	toggleCMSContent: function (el, event) {
		//We always hide any modal content before showing CMS content
		if(this.modalTarget) {
			this.modalTarget.setStyle('display','none');
			this.overlay.setStyle('display','none');
		}
		//Hide any current CMS content if ids do not match
		var content = $('dashboard-content-' + el.getAttribute('data-target'));
		if(this.contentTarget !== null && this.contentTarget.id !== content.id) {
			this.contentTarget.addClass('collapse');
		}
		//Show CMS content and save reference
		if(content.hasClass('collapse')) {
			content.removeClass('collapse');
			this.contentTarget = content;
		//Hide CMS content and clear reference
		} else {
			content.addClass('collapse');
			this.contentTarget = null;
		}
		if(event) {
			event.preventDefault();
			event.stopPropagation();
		}
	},
	toggleEditor: function (open, content) {
		if(open) {
			this.editor.setData(content);
			$('dashboard-overlay').setStyle('display','block');
			$('dashboard-modal-editor').setStyle('display','block');
		} else {
			this.editor.setData('');
			this.editorTarget = null;
			$('dashboard-overlay').setStyle('display','none');
			$('dashboard-modal-editor').setStyle('display','none');
		}
		return false;
	},
	saveEditor: function() {
		var self = this;
		var content = this.editor.getData();
		this.editorTarget.innerHTML = content;
		var saveEditor = new Request({
			url: this.editorTarget.getAttribute('data-url'),
			data: {
				id: this.editorTarget.getAttribute('data-id'),
				sColumn: this.editorTarget.getAttribute('data-column'),
				sValue: content,
				//TODO: de-hardcode iLocaleId
				iLocaleId: 116,
				bLocale: this.editorTarget.getAttribute('data-locale')
			},
			onSuccess: function() {
				this.toggleEditor(false,'');
			}.bind(self)
		});
		saveEditor.send();
	}
});
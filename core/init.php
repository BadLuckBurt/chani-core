<?php

	namespace Core;

	class Module extends \Core\ModuleTemplate
	{
		public function registerAutoloaders() {}

		public function registerServices($oDi, $sNameSpace = '', $bCore = true)
		{
			parent::registerServices($oDi,__NAMESPACE__,false);
		}
	}

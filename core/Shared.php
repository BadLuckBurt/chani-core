<?php

	namespace Core;

	use \Phalcon\Text;

	class Shared {

		/**
		 * @static
		 * @param array $aModuleNames
		 * @return array
		 * Constructs an array for loading the modules defined in index.php
		 * TODO: This will need to be loaded from the database in the future
		 */
		public static function buildModuleArray($aModuleNames = array()) {
			$aModules = array();
			$aModules['core'] = array(
				'className' => 'Core\Module',
				'path'      => BASE_DIR.'/core/init.php'
			);

			foreach($aModuleNames AS $sModuleName) {
				$sModuleClassName = Text::camelize($sModuleName).'\Module';
				$aModules[$sModuleName] = array(
					'className' => $sModuleClassName,
					'path'      => MODULE_DIR.'/'.$sModuleName.'/init.php'
				);
			}
			return $aModules;
		}

		/**
		 * @static
		 * @param array $aModuleNames
		 * @return array
		 * Constructs the default namespaces for each module, includes Core by default
		 */
		public static function buildNamespaces($aModuleNames = array()) {

			$aNamespaces = array(
				'Core'                  => BASE_DIR.'/core/',
				'Core\Classes'          => BASE_DIR.'/core/classes/',
				'Core\Controllers'  => BASE_DIR.'/core/controllers/',
				'Core\Models'       => BASE_DIR.'/core/models/',
				'Core\Controllers\Chani'  => BASE_DIR.'/core/controllers/chani/',
				'Core\Models\Chani'       => BASE_DIR.'/core/models/chani/'
			);

			foreach($aModuleNames AS $sModuleName) {

				$sDir = MODULE_DIR.'/'.$sModuleName;
				$sModule = Text::camelize($sModuleName);

				$aNamespaces[$sModule.'\Controllers']  = $sDir.'/controllers/';
				$aNamespaces[$sModule.'\Models']       = $sDir.'/models/';

				$aNamespaces[$sModule.'\Controllers\Chani']  = $sDir.'/controllers/chani/';
				$aNamespaces[$sModule.'\Models\Chani']       = $sDir.'/models/chani/';

			}

			return $aNamespaces;

		}

		/**
		 * @static
		 * @param array $aModuleNames
		 * @return array|mixed
		 * Loads the route groups for all modules into an array for mounting in a Router object
		 */
		public static function loadRouteGroups($aModuleNames = array()) {
			$aRouteGroups = array();

			foreach($aModuleNames AS $sModuleName) {
				$aRouteGroups[] = require_once(MODULE_DIR.'/'.$sModuleName.'/routes/'.$sModuleName.'Routes.php');
			}

			$aRouteGroups[] = require_once(BASE_DIR.'/core/routes/coreRoutes.php');
			return $aRouteGroups;
		}

		/**
		 * @param $sHtml
		 * @param bool $sHtml5
		 * @return string
		 * Cleans HTML output, taking HTML5 in consideration if specified
		 */
		public static function tidy($sHtml, $sHtml5 = false) {
			$tidyConfig = array(
				'indent' => true,
				'clean' => false,
				'output-xhtml' => false,
				'wrap' => 0,
				'new-blocklevel-tags' => 'article aside audio details figcaption figure footer header hgroup main nav section source summary temp track video',
				'new-empty-tags' => 'command embed keygen source track wbr',
				'new-inline-tags' => 'audio canvas command datalist embed keygen mark meter output progress time video wbr'
			);

			if($sHtml5 === true) {
				$tidyConfig['doctype'] = 'omit';
			}

			$tidy = tidy_parse_string($sHtml, $tidyConfig, 'UTF8');
			$tidy->cleanRepair();
			$sTidy = (string) $tidy;

			if($sHtml5 === true) {
				$sTidy = '<!DOCTYPE html>'.$sTidy;
			}

			return $sTidy;

		}

		/**
		 * @return false|string
		 * Returns a date for created / updated database records
		 */
		public static function getDBDate() {
			return date('YmdHis');
		}

		/**
		 * @param $aModuleNames
		 * @return bool
		 * Registers the specified modules so they can be loaded when a route is matched
		 */
		public static function registerModules($aModuleNames) {
			$oModuleController = new \Core\Controllers\Chani\ModuleController();
			return $oModuleController->register($aModuleNames);
		}

	}
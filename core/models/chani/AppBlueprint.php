<?php

	namespace Core\Models\Chani;

	use Phalcon\Mvc\Model;
	use Phalcon\Mvc\Model\Validator\Uniqueness;
	use Phalcon\Text AS Text;

	class AppBlueprint extends Model {

		public $sFallBackNameSpace = '\Chani'; //The fallback namespace
		public $sModel;
		public $_model ='AppBlueprint';
		public $_modelLower;
		public $bSpice = true;

		/**
		 * @param $sNameSpace
		 * @param $sClass
		 * @return string
		 * Test if a class has been extended, if not, it returns the \Chani fallback class
		 */
		public function testClass($sNameSpace, $sClass) {
			//Strip the fallback namespace from the real namespace to check for an extended class
			$sTmpNameSpace = str_replace($this->sFallBackNameSpace,'',$sNameSpace);
			//Construct the class name to be checked
			$sTest = $sTmpNameSpace.'\\'.$sClass;
			//If the class doesn't exists, add the fallback namespace
			if(class_exists($sTest) === false) {
				$sTest = $sTmpNameSpace.$this->sFallBackNameSpace.'\\'.$sClass;
			}
			return $sTest;
		}

		/**
		 * @return string
		 * Returns the table name in lowercase, important on CaseSensitive file systems
		 */
		public function getSource() {
			return 'blueprint';
		}

		/**
		 * This function is called the first time the class for the model is loaded into memory
		 */
		public function initialize() {
			$this->_modelLower = strtolower($this->_model);
			//Set the relation to spice content blocks, bSpice is true by default but can be overridden
			if($this->bSpice) {
				$sClass = $this->testClass('Spice\Models\Chani', 'AppSpice');
				$this->hasMany(
					'id',
					$sClass,
					'iModelId',
					array('alias' => 'spice')
				);
			}
		}
	}
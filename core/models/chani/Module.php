<?php

	namespace Core\Models\Chani;

	use \Core\Shared;

	class Module extends CmsBlueprint {

		public $dtCreated;
		public $dtUpdated;

		public $sFallBackNameSpace = '\Chani'; //The fallback namespace
		public $sModel;
		public $sFolderName;
		public $iSequence;
		public $bActive;
		public $_model ='Module';
		public $_modelLower;

		public function getSource() {
			return 'module';
		}

		public function initialize() {
			parent::initialize();
		}

		/**
		 * @param $sFolderName
		 * @param $iSequence
		 * @param $bActive
		 * @return Module
		 */
		public static function add($sFolderName, $iSequence, $bActive) {
			$oModule = new Module();
			$oModule->dtCreated = Shared::getDBDate();
			$oModule->sFolderName = $sFolderName;
			$oModule->iSequence = $iSequence;
			$oModule->bActive = $bActive;
			$oModule->save();
			return $oModule;
		}

		/**
		 * @param null $aData
		 * @param null $aWhiteList
		 * @return bool
		 */
		public function save($aData = null, $aWhiteList = null) {
			$this->dtUpdated = Shared::getDBDate();
			return parent::save();
		}
	}
<?php

	namespace Core\Models\Chani;

	use Phalcon\Mvc\Model,
		Phalcon\Mvc\Model\Validator\Uniqueness,
		Phalcon\Mvc\Model\Message,
		\Core\Shared;
	use Phalcon\Text AS Text;

	class CmsBlueprint extends Model {
		//Default columns, present in every table
		public $dtCreated;
		public $dtUpdated;

		public $sFallBackNameSpace = '\Chani'; //The fallback namespace
		public $sModel;
		public $_model ='CmsBlueprint';
		public $_modelLower;
		public $bSpice = true;

		//TODO: Implement Phalcon validation instead of only checking for non NULL values
		public function validation() {
			$notNullAttributes = $this->getModelsMetaData()->getNotNullAttributes($this);
			foreach ($notNullAttributes as $field) {
				if($field !== 'id') {
					if (!isset($this->$field) || $this->$field === null) {
						$oMessage = new Message($field.' is required');
	                    $this->appendMessage($oMessage);
	                    return false;
	                }
				}
			}
			return true;
		}

		/**
		 * @param string $sNameSpace
		 * @param string $sClass
		 * @return string
		 * Tests to see if an extended class exists for the model calling
		 * If not extended class is found, the default class is loaded
		 */
		public function testClass($sNameSpace = '', $sClass = '') {

			if($sNameSpace == '') $sNameSpace = __NAMESPACE__;
			if($sClass == '') $sNameSpace = 'Blueprint';

			//Strip the fallback namespace from the real namespace to check for an extended class
			$sTmpNameSpace = str_replace($this->sFallBackNameSpace,'',$sNameSpace);

			//Construct the class name to be checked
			$sTest = $sTmpNameSpace.'\\'.$sClass;

			//If the class doesn't exist, add the fallback namespace
			if(class_exists($sTest) === false) {
				$sTest = $sTmpNameSpace.$this->sFallBackNameSpace.'\\'.$sClass;
			}

			return $sTest;

		}

		/**
		 * @return string
		 * Returns the tablename in lowercase, important for CaseSensitive file systems
		 */
		public function getSource() {
			return 'blueprint';
		}

		/**
		 * This function is called the first time the class for the model is loaded into memory
		 */
		public function initialize() {
			$this->_modelLower = strtolower($this->_model);

			if($this->bSpice) {
				$sClass = $this->testClass('Spice\Models\Chani','CmsSpice');
				$this->hasMany(
					'id',
					$sClass,
					'iModelId',
					array('alias' => 'spice')
				);
			}

		}

		/**
		 * @param bool $bSaveSpice
		 * @return bool
		 * Saves the data change by the user after editing
		 */
		public function saveData($bSaveSpice = true) {
			//Set the date / time of update
			$this->dtUpdated = Shared::getDBDate();
			//If there is a relation between this model and the spice module, save the spice models and it's locales
			if($bSaveSpice === true) {
				//Check for the existence of editable spice
				$oEditableSpice = $this->getEditableSpice(0, false);
				//Only process if the editable spice exists
				if($oEditableSpice) {
					$key = 0;
					foreach($oEditableSpice AS $oEditable) {
						$this->processSpice($oEditable, $key);
						$key++;
					}
				}
			}
			//Save the data for this model and return the result (success or error)
			return $this->save();
		}

		/**
		 * @param $oEditable
		 * @param int $key
		 * Save the edited spice and removes the original records from the database
		 */
		public function processSpice($oEditable, $key = 1) {
			//TODO: Find out why we're checking for 'spice' here, must be the automatic relation
			//TODO: Think this is because of ->save()?
			if($key == 0 && $oEditable->_model !== 'spice') {
				//Check for records with the original values
				$oOriginal = $oEditable::find(array(
					'conditions' => 'sModel = :sModel: AND iModelId = :iModelId: AND bEdit = 0',
					'bind' => array(
						'sModel' => $oEditable->sModel,
						'iModelId' => $oEditable->iModelId
					)
				));
				//Delete the found original records
				foreach($oOriginal AS $original) {
					$original->delete();
				}
			}
			//Delete the editable spice
			if($oEditable->bDeleted == 1) {
				if($oEditable->delete() !== false) {

				} else {
					echo('error deleting spice');
					die;
				}
			//Save the editable spice
			} else {
				//Make this record the new 'original' record
				$oEditable->saveMedia();
				$oEditable->bEdit = 0;
				if($oEditable->save() !== false) {

				} else {
					echo('error saving spice');
					die;
				}
			}
		}

		/**
		 * @return mixed
		 * Returns the live locales for a model (the records not being edited)
		 */
		public function getLiveLocales() {
			$oLocales = $this->getLocales(array(
				'conditions' => 'bEdit = 0'
			));
			return $oLocales;
		}

		/**
		 * @return mixed
		 * Returns the editable locales for a model (the records being edited)
		 */
		public function getEditableLocales() {
			$oLocales = $this->getLocales(array(
				'conditions' => 'bEdit = 1'
			));
			return $oLocales;
		}

		/**
		 * @param int $bFixed
		 * @param bool $bExcludeDeleted
		 * @return mixed
		 * Returns the live blocks for a model (content not being edited)
		 * Can include or exclude fixed and / or deleted blocks
		 */
		public function getLiveSpice($bFixed = 0, $bExcludeDeleted = true) {
			$sConditions = 'bEdit = 0 AND bFixed = :bFixed: AND sModel = :sModel:';
			if($bExcludeDeleted == true) {
				$sConditions .= ' AND bDeleted = 0';
			}
			$oSpice = $this->getSpice(array(
				'conditions' => $sConditions,
				'bind' => array(
					'bFixed' => $bFixed,
					'sModel' => $this->getSource()
				),
				'order' => 'iSequence ASC'
			));
			return $oSpice;
		}

		/**
		 * @param int $bFixed
		 * @param bool $bExcludeDeleted
		 * @return mixed
		 * Returns the editable spice for a model (content that's being edited)
		 * Can include or exclude fixed and / or deleted spice blocks
		 */
		public function getEditableSpice($bFixed = 0, $bExcludeDeleted = true) {
			$sConditions = 'bEdit = 1 AND bFixed = :bFixed: AND sModel = :sModel:';
			if($bExcludeDeleted == true) {
				$sConditions .= ' AND bDeleted = 0';
			}
			$oSpice = $this->getSpice(array(
				'conditions' => $sConditions,
				'bind' => array(
					'bFixed' => $bFixed,
					'sModel' => $this->getSource()
				),
				'order' => 'iSequence ASC'
			));
			return $oSpice;
		}
	}
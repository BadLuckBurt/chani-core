<?php

	$aMessages = array(
		'module' => 'Core',
		'sTitle' => 'Module',
		'confirmModuleDeactivation' => 'Click OK to deactivate the following module: ',
		'delete' => 'Delete',
		'move' => 'Move',
		'edit' => 'Edit',
		'cancel' => 'Back to overview',
		'reset' => 'Reset',
		'sAlt'      => 'Alt text',
		'sContent'  => 'Description',
		'sUrl'      => 'Video url',
		'save'      => 'Save',
		'processUrl'      => 'Process URL',
		'addFile' => 'Add file',
		'uploadFiles' => 'Upload files',
		'cropper' => 'Open cropper'
	);
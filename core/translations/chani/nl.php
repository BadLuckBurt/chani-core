<?php

	$aMessages = array(
		'module' => 'Core',
		'sTitle' => 'Module',
		'confirmModuleDeactivation' => 'Klik op OK om de volgende module uit te schakelen: ',
		'delete' => 'Verwijderen',
		'move' => 'Verplaatsen',
		'edit' => 'Bewerken',
		'cancel' => 'Terug naar overzicht',
		'reset' => 'Herstellen',
		'sAlt'      => 'Alt tekst',
		'sContent'  => 'Omschrijving',
		'sUrl'      => 'Video url',
		'save'      => 'Opslaan',
		'processUrl' => 'Verwerk URL',
		'addFile' => 'Voeg bestand toe',
		'uploadFiles'=> 'Upload bestanden',
		'cropper' => 'Open cropper'
	);
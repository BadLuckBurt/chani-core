<?php

	namespace Core;

	use \Phalcon\Mvc\Router\Group;

	class Routes extends Group
	{

		public function initialize()
		{

			$sFallback = '\Chani';
			$sNameSpaceLower = strtolower(__NAMESPACE__);

			$sControllerNameSpace = __NAMESPACE__.'\Controllers'.$sFallback;

			//Default paths
			$this->setPaths(array(
				'module' => $sNameSpaceLower
			));

			$this->add('/chani/'.$sNameSpaceLower.'/:controller/:params',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 1,
				'action'        => 'index',
				'params'        => 2
			));

			$this->add('/chani/module/:params',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 'Module',
				'action'        => 'index',
				'params'        => 1
			));

			$this->add('/chani/module/:action/:params',array(
				'namespace'     => $sControllerNameSpace,
				'controller'    => 'Module',
				'action'        => 1,
				'params'        => 2
			));

		}

	}

	return new Routes();
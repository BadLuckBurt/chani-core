<?php

	namespace Core\Controllers\Chani;

	use \Phalcon\Tag,
		\Core\Shared AS Shared,
		\Core\Models\Chani\Module;

	class ModuleController extends CmsController {

		/**
		 * @return bool|\Phalcon\Mvc\View
		 * Displays an overview of module, both active and inactive
		 */
		public function indexAction() {
			$aItems = $this->buildOverview();
			$aButtons = array(
				'bAdd' => false,
				'bEdit' => true,
				'bDelete' => true,
				'bMove' => false,
				'bAddSub' => false
			);
			$sOverview = $this->renderOverview($aItems, 'module', $this->_getTranslation(__NAMESPACE__), $aButtons);

			$this->addOverviewCSS();
			$this->addOverviewJS('module');
			$sHead = $this->renderHead();

			$sBody = $this->renderBody($sOverview, false);
			$sHtml = $this->renderHTML('Modules',$sHead, $sBody, true);
			return $sHtml;
		}

		/**
		 * Edit action to manage each module's options / settings (if any)
		 */
		public function editAction() {
			echo('this is the ModuleController edit action');
			die;
		}

		/**
		 * Save module options / settings and redirect to index
		 */
		public function saveAction() {
			echo('this is the ModuleController save action');
			die;
		}

		/**
		 * Disables a module to prevent it from being accessible
		 */
		public function disableAction() {
			echo('this is the ModuleController disable action');
			die;
		}

		/**
		 * Install a new module
		 */
		public function installAction() {
			echo('this is the ModuleController install action');
			die;
		}

		/**
		 * @param $sModule
		 * @return string
		 * Get the language dependent name for a module, allows for better description
		 */
		public function getModuleName($sModule) {
			$oTranslator = $this->_getTranslation($sModule.'\cms');
			return $oTranslator->_('module');
		}

		/**
		 * @return array
		 * Gather module information for the Overview list
		 */
		public function buildOverview() {

			$oModules = Module::find(array(
				'conditions' => 'bActive = 1',
				'order' => 'iSequence ASC'
			));

			$oTranslator = $this->_getTranslation(__NAMESPACE__);
			$aItems = [];
			foreach($oModules AS $oModule) {
				$sModuleName = $this->getModuleName($oModule->sFolderName);
				$aItems[] = array(
					'id' => $oModule->id,
					'iSequence' => $oModule->iSequence,
					'sTitle' => $oModule->sFolderName.' - '.$sModuleName,
					'class' => 'module',
					'sub' => array(),
					'addUrl' => 'chani/module/add/'.$oModule->id,
					'editUrl' => 'chani/module/edit/'.$oModule->id,
					'deleteUrl' => 'chani/module/delete/'.$oModule->id,
					'confirmDelete' => $oTranslator->_('confirmModuleDeactivation').' '.$sModuleName
				);
			}
			return $aItems;
		}

		/**
		 * @param $aModules
		 * @return bool
		 * Clear all modules from the database and re-reads the modules folder
		 */
		//TODO: Make clear optional to preserve module options / settings
		public function register($aModules) {
			$aConfig = $this->getDI()->get('config');
			$oDB = new \Phalcon\Db\Adapter\Pdo\Mysql(array(
				'host' => $aConfig->database->local->host,
				'username' => $aConfig->database->local->username,
				'password' => $aConfig->database->local->password,
				'dbname' => $aConfig->database->local->dbname,
				'charset'   =>'utf8'
			));
			$oDB->connect();
			$oDB->execute('TRUNCATE TABLE module');

			for($i = 0; $i < count($aModules); $i++) {
				$sSQL = 'INSERT INTO module (dtCreated, dtUpdated, sFolderName, iSequence, bActive) VALUES(:dtCreated,:dtUpdated,:sFolderName,:iSequence,:bActive)';
				$oDB->execute($sSQL,array(
					'dtCreated' => Shared::getDBDate(),
					'dtUpdated' => Shared::getDBDate(),
					'sFolderName' => $aModules[$i],
					'iSequence' => $i + 1,
					'bActive' => 1
				));
			}
			return true;
		}

		/**
		 * @param $sModule
		 * Removes any module related data in the public/modules folder
		 * and clears related data - might need to be module dependent
		 */
		//TODO: Implement cleanup function
		public function cleanup($sModule) {

		}
	}
<?php

	namespace Core\Controllers\Chani;

	use \Phalcon\Mvc\Controller,
		\Phalcon\Translate\Adapter\NativeArray AS Translator,
		\Phalcon\Mvc\View\Simple;

	class AppController extends Controller {
		//Fallback namespace is used when no extended translation file is found
		public $sFallBackNameSpace = '\Chani';

		//Store the subview for rendering without caching
		public $subview = null;

		/**
		 * @param $sDir
		 * @return null|Simple
		 * Create a view engine without caching enabled
		 */
		public function getSubView($sDir) {
			if($this->subview === null) {
				$oSubView = new Simple();
				$oSubView->setDI($this->di);
				$oSubView->setViewsDir(MODULE_DIR.'/'.$sDir.'/views/');
				$oSubView->registerEngines(array(
					".volt" => function ($view, $di) {
						$volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
						return $volt;
					}
					//".volt" => 'Phalcon\Mvc\View\Engine\Volt'
				));
				$this->subview = $oSubView;
			}
			return $this->subview;
		}

		/**
		 * Placeholder index action, controllers that extend this class should overwrite this
		 */
		public function indexAction() {
			echo('This is the '.get_class($this));
		}
		/**
		 * @return \Phalcon\Translate\Adapter\NativeArray
		 * Gets the translations for the language the user is currently using
		 * TODO: Tweak this function to handle non-existing translation locales and force it to use 'nl' instead of 'nl-NL'
		 */
		public function _getTranslation($sNameSpace = '')
		{
			//Determine the browser language of the user
			$sLanguage = $this->request->getBestLanguage();
			//Format returned for example: en-EN - we only need the first part
			$aLanguage = explode('-',$sLanguage);
			$sLanguage = $aLanguage[0];

			//If no namespace is provided, use the current one
			if($sNameSpace == '') {
				$sNameSpace = __NAMESPACE__;
			}
			//Build the regular path, the chani path and fallback (english) path using the namespace,
			$aNameSpace = explode('\\',$sNameSpace);
			$sPath = MODULE_DIR.'/'.strtolower($aNameSpace[0]).'/translations/';
			$sChaniPath = $sPath.'chani/';
			$sFallback = $sChaniPath.'en.php';
			$sPath .= $sLanguage.'.php';
			$sChaniPath .= $sLanguage.'.php';

			//Check if we have a translation file for that language, if not fall back to whatever is available
			$aMessages = null;
			if (file_exists($sPath)) {
				require $sPath;
			} elseif(file_exists($sChaniPath)) {
				// fallback to some default
				require $sChaniPath;
			} else {
				require $sFallback;
			}
			//Return a translation object
			return new Translator(array("content" => $aMessages));
		}
	}
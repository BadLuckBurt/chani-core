<?php

	namespace Core\Controllers\Chani;

	use \Phalcon\Mvc\Controller,
		\Phalcon\Translate\Adapter\NativeArray AS Translator,
		\Phalcon\Http\Response,
		\Phalcon\Tag,
		\Core\Shared AS Shared,
		\User\Models\Chani\CmsUser AS CmsUser;

	class CmsController extends Controller {
		//Currently loaded models - should be taken from config / database
		//TODO: Should be handled automatically in the future by installing modules
		public $aModules = [
			'page' => [
				'id' => 'pages',
				'href' => 'chani/page/',
				'innerHTML' => 'Pages',
				'class' => '',
				'iconClass' => 'fa-globe',
				'title' => 'Manage site pages',
				'sub' => [
					[
						'id' => 'sitemap',
						'href' => 'chani/page/',
						'target' => 'sitemap',
						'title' => 'Sitemap',
						'class' => 'fa-bars'
					],
					[
						'id' => 'media-library',
						'href' => 'chani/page/',
						'target' => 'library',
						'title' => 'Media library',
						'class' => 'fa-image'
					]
				]
			],
			'user' => [
				'id' => 'user',
				'href' => 'chani/user',
				'innerHTML' => 'Users',
				'class' => '',
				'iconClass' => 'fa-users',
				'title' => 'Manage users',
				'sub' => [
					[
						'id' => 'manageusers',
						'href' => 'chani/user',
						'target' => 'manageusers',
						'title' => 'User management',
						'class' => 'fa-users'
					]
				]
			]
		];

		//Used for swapping view dirs in the render functions
		public $sTmpViewDir = '';
		public $sViewDir = '';

		//Contains the meta tags to be processed in renderHead()
		public $aMeta = array(
			'name' => array(
				'charset' => 'UTF-8',
				'keywords'=> 'your, tags',
				'description' => '150 words',
				'language' => 'NL',
				'robots' => 'index,follow'
			),
			'http-equiv' => array(
				'Expires' => 0,
				'Pragma' => 'no-cache',
				'Cache-Control' => 'no-cache',
				'imagetoolbar' => 'no',
				'x-dns-prefetch-control' => 'off'
			)
		);

		//Contains the Javascript to be processed in renderHead()
		public $aScripts = array(
			'public/core/js/mootools-core.js',
			'public/core/js/mootools-more.js',
			'public/core/js/ckeditor/ckeditor.js',
			'public/core/js/cms/chani/ChaniDragDrop.js',
			'public/core/js/cms/chani/ChaniResizer.js',
			'public/core/js/cms/chani/Chani.js',
			'public/core/js/cms/chani/Blueprint.js'
		);

		//Contains the events to occur on page load, processed in renderHead()
		public $aDomready = array(
			"Chani = new ChaniDashboard();"
		);

		//Contains the external CSS files to be loaded, processed in renderHead()
		public $aCss = [
			'public/core/css/chani/dashboard.css',
			'public/core/css/chani/dashboard-list.css',
			'public/core/css/chani/uniform.css',
			'public/core/css/chani/image-cropper.css',
			'public/core/css/font-awesome.min.css'
		];

		//Contains any inline CSS, processed in renderHead()
		public $aCssInline = array();

		//The form fields for the module, processed in prepareFormFields()
		public $aFormFields = [];

		//If no extended controller, model or class is found
		// in <MODULE>\Controllers, <MODULE>\Models or <MODULE>\Classes
		// \Chani is appended to fallback to default controllers, models or classes
		public $sFallBackNameSpace = '\Chani';

		//Executes before every matched route action for the CMS
		public function beforeExecuteRoute() {
			//Redirect if there is no valid login section
			if($this->userIsLoggedIn() === false) {
				$response = new Response();
				$response->redirect('chani/login/');
				$response->send();
			} else {
				//TODO: Re-implement ACL checks
			}
		}

		//Renders the form fields for a model's editing form
		//TODO: Obsolete, remove once all modules have been ported over to the single form setup
		public function prepareFormFields($oModel, $oEditables, $oTranslator = null) {
			$oTag = new Tag();
			//If no translator object is supplied, retrieve the default translations for this module
			if($oTranslator === null) {
				$oTranslator = $this->_getTranslation();
			}
			//Use the first available language dependent record
			if(count($oEditables) > 0) {
				$oEditable = $oEditables[0];
			//No editable records means we're editing a model without locale fields
			} else {
				$oEditable = null;
			}
			//Render the HTML for each formfield
			for($i = 0; $i < count($this->aFormFields); $i++) {
				//Check the fields in each form row
				foreach($this->aFormFields[$i]['fields'] AS $key => $aField) {
					//Get the HTML tag to be rendered for this field
					$sTag = $aField['tag'];
					//Set id and name if they haven't already been set from the controller
					if(!isset($aField['id'])) $aField['id'] = $key.'_'.$oModel->id;
					if(!isset($afield['name'])) $aField['name'] = $key;
					//Clear the tag and html to prevent them from being rendered in the HTML later
					unset($aField['tag']);
					unset($aField['html']);

					//Determine the source object, data-locale means the field contains language dependent value
					if($aField['data-locale'] == 'true') {
						$oSource = $oEditable;
					} else {
						$oSource = $oModel;
					}
					//Set the field's value if it hasn't already been set
					if($aField['value'] == '') {
						$aField['value'] = $oSource->$key;
					}
					//Prepare the checked attribute for checkboxes
					if($sTag == 'checkField') {
						if($oSource->$key == 1) {
							$aField['checked'] = 'checked';
						}
					}
					//Set the data attributes required for Javascript event assignment
					//when the full page has been rendered
					$aField['data-id'] = $oModel->id;
					$aField['data-column'] = $key;
					//Set the label name for this field
					$this->aFormFields[$i]['fields'][$key]['label'] = $oTranslator->_($key);
					//Select tags are easier to generate manually, offering more control for the data structure used
					//TODO: Should be split off to a separate function for easier customisation
					if($sTag == 'selectStatic') {
						$aValues = $aField['values'];
						$sHTML = '<select data-id="'.$oModel->id.'" data-column="'.$key.'" class="'.$aField['class'].'" name="'.$key.'" data-events="'.$aField['data-events'].'" data-change="'.$aField['data-change'].'" data-locale="'.$aField['data-locale'].'">'."\n";
						foreach($aValues AS $val => $name) {
							$sHTML .= '<option value="'.$val.'">'.$name.'</option>'."\n";
						}
						$sHTML .= '</select>';
						$aField['name'] = $key;
					//Other form tags can be generated with Phalcon's Tag class.
					} else {
						$sHTML = $oTag->$sTag($aField);
						if($sTag == 'checkField' || $sTag == 'radioField') {
							$sHTML = '<div class="boxes">'.$sHTML.'</div>';
						}
					}
					//Save the resulting HTML to the form fields array
					$this->aFormFields[$i]['fields'][$key]['html'] = $sHTML;
				}
			}
		}

		/**
		 * @return \Phalcon\Mvc\Model\Resultset|\Phalcon\Mvc\Phalcon\Mvc\Model|string
		 * Returns the name of the logged in user or an error message containing the user id
		 */
		public function getUserName() {
			$iUserId = $this->getUserId();
			$oUser = CmsUser::findFirst($iUserId);
			if($oUser) {
				return $oUser->sUserName;
			} else {
				return 'No user found for id: '.$iUserId;
			}
		}

		/**
		 * @return bool
		 * Checks the session to see if the user is logged in
		 */
		public function userIsLoggedIn() {
			if(
				$this->session->get('loginSuccess') == true
				&& (int) $this->session->get('userId') > 0
			) {
				return true;
			}
			return false;
		}

		/**
		 * @return int|mixed
		 * Returns the id of the user currently logged in or -1 if no login was found
		 */
		public function getUserId() {
			if($this->userIsLoggedIn()) {
				return $this->session->get('userId');
			} else {
				return -1;
			}
		}

		/**
		 * @param $aItems
		 * @param $sModule
		 * @param null $oTranslator
		 * @param null $aButtons
		 * @return bool|\Phalcon\Mvc\View
		 * Renders a list of items that can have several actions assigned, an example can be found in the Page module
		 */
		public function renderOverview($aItems, $sModule, $oTranslator = null, $aButtons = null, $aParentIds = array()) {
			if($aButtons === null) {
				$aButtons = array(
					'bAdd' => true,
					'bEdit' => true,
					'bDelete' => true,
					'bMove' => true
				);
			}
			if($oTranslator === null) {
				$oTranslator = $this->_getTranslation();
			}
			$sOverviewBody = $this->renderOverviewBody($aItems, $oTranslator, $aButtons, $sModule, $aParentIds, false);
			$this->swapViewDir();
			$sOverview = $this->view->render('chani/overview',array(
				'overview' => $sOverviewBody,
				't' => $oTranslator,
				'button' => $aButtons,
				'module' => $sModule
			));
			$this->restoreViewDir();
			return $sOverview;
		}

		/**
		 * @param $aItems
		 * @param $oTranslator
		 * @param $aButtons
		 * @param string $sModule
		 * @param bool $bSub
		 * @param string $sOverviewBody
		 * @return string
		 * Renders the body for the overview
		 */
		public function renderOverviewBody($aItems, $oTranslator, $aButtons, $sModule = 'core', $aParentIds, $bSub = false, $sOverviewBody = '') {
			//The Overview volt templates are part of the Core module so we swap view dirs
			//in the controller calling this function.
			//We also add the overlay HTML and open the ul tag
			if($bSub === false) {
				$this->swapViewDir();
				$sOverviewBody .= '<div class="overlay"></div><ul id="'.$sModule.'Overview">';
			}
			//Loop through the items
			for($i = 0; $i < count($aItems); $i++) {
				//Calculate the sequence for each item, starts at 1
				$iSequence = $i + 1;
				$sOverviewRow = $this->renderOverViewRow($aItems[$i], $aButtons, $iSequence, $oTranslator, $sModule, $aParentIds);
				$sOverviewBody .= $sOverviewRow;
			}
			//Close the ul tag and restore the original view dir of the calling controller
			if($bSub === false) {
				$sOverviewBody .= '</ul>';
				$this->restoreViewDir();
			}
			return $sOverviewBody;
		}

		/**
		 * @param $aItem
		 * @param $aButtons
		 * @param $iSequence
		 * @param $oTranslator
		 * @param $sModule
		 * @return string
		 * Renders the HTML for a single item in the overview
		 */
		public function renderOverviewRow($aItem, $aButtons, $iSequence, $oTranslator, $sModule, $aParentIds) {
			//Open the li-tag for this item
			$sOverviewRow = '<li data-target="'.$aItem['data-target'].'" id="row_'.$aItem['id'].'" data-id="'.$aItem['id'].'">';
			//Render the HTML for the item
			$sOverviewRow .= $this->view->render('chani/overviewRow', array(
				'button' => $aButtons,
				'item' => $aItem,
				'sequence' => $iSequence,
				't' => $oTranslator,
				'module' => $sModule,
				'count' => count($aItem['sub'])
			));
			if(in_array($aItem['id'],$aParentIds)) {
				$dataToggle = ' data-toggle="open"';
			} else {
				$dataToggle = '';
			}

			//Then render the rows for it's sub items
			$sOverviewRow .= '<ul'.$dataToggle.' id="sub-sortables-'.$aItem['id'].'">';
			$sOverviewRow .= $this->renderOverviewBody($aItem['sub'], $oTranslator, $aButtons, $sModule, $aParentIds, true, '');
			$sOverviewRow .= '</ul>';
			$sOverviewRow .= '</li>';
			return $sOverviewRow;
		}

		/**
		 * @return bool|\Phalcon\Mvc\View
		 * Renders the HTML for the HEAD tag, META tags, Javascript, CSS and DOM load events are added here
		 */
		public function renderHead() {
			//Swap to the Core module's views
			$this->swapViewDir();
			//Render the HTML, restore the view dir of the calling controllers and return HTML
			$sHead = $this->view->render('chani/head',array(
				'meta' => $this->aMeta,
				'scripts' => $this->aScripts,
				'css' => $this->aCss,
				'cssInline' => $this->aCssInline,
				'domready' => $this->aDomready
			));
			$this->restoreViewDir();
			return $sHead;
		}

		/**
		 * @param $sBody
		 * @param $sitemap
		 * @param array $dashboardModals
		 * @param bool $bAddPreview
		 * @return bool|\Phalcon\Mvc\View
		 * Render the HTML for the body tag
		 */
		public function renderBody($sBody, $dashboardContents = [], $dashboardModals = [], $bAddPreview = false) {
			//Swap to the Core module's views
			$this->swapViewDir();
			//Retrieve user related variables
			$bUserIsLoggedIn = $this->userIsLoggedIn();
			$iUserId = $this->getUserId();
			$sUserName = $this->getUserName();

			//TODO: Load font family and styles for CKEditor
			//Renders a CKEditor to be used in a modal view, only one editor is loaded for all text editing
			//to keep things light-weight
			$dashboardModals['editor'] = ['html' => $this->view->render('chani/dashboard.editor'),'buttons' => ''];

			//Render the dashboard navigation for the loaded modules
			$nav = $this->view->render('chani/dashboard.nav',[
				'modules' => $this->aModules
			]);

			//$cUserController = new UserController();
			//$sUserList = $cUserController->getUserList();

			//Render the contents for dashboard overlays
			$dbContents = [];
			$dbContents = array_merge($dbContents, $dashboardContents);

			//Render the entire HTML for the body-tag, restore the view and return the HTML
			$sBody = $this->view->render('chani/dashboard',array(
				'nav' => $nav,
				'dashboardContents' => $dbContents,
				'dashboardModals' => $dashboardModals,
				'content' => $sBody,
				'userIsLoggedIn' => $bUserIsLoggedIn,
				'addPreview' => $bAddPreview,
				'userId' => $iUserId,
				'userName' => $sUserName
			));

			$this->restoreViewDir();
			return $sBody;
		}

		/**
		 * @param $sTitle
		 * @param $sHead
		 * @param $sBody
		 * @param bool $bTidy
		 * @return bool|\Phalcon\Mvc\View
		 * Renders the HTML5 template with title, head, body and (optional) cleans the result using Tidy
		 */
		public function renderHTML($sTitle, $sHead, $sBody, $bTidy = false) {
			$this->swapViewDir();
			$sHTML = $this->view->render('chani/html5',array(
				'title' => $sTitle,
				'head' => $sHead,
				'body' => $sBody
			));
			$this->restoreViewDir();
			if($bTidy == true) {
				$sHtml = Shared::tidy($sHTML, true);
			}
			return $sHTML;
		}

		/**
		 * Stores the view dir of the calling controller and forces the view dir to the Core module
		 */
		public function swapViewDir() {
			if($this->sTmpViewDir == '') {
				$this->sViewDir = BASE_DIR.'/core/views/';
				$this->sTmpViewDir = $this->view->getViewsDir();
				$this->view->setViewsDir($this->sViewDir);
			}
		}

		/**
		 * Restores the view dir to the original of the calling controller and empties the tmp view dir
		 */
		public function restoreViewDir() {
			if($this->sTmpViewDir != '') {
				$this->view->setViewsDir($this->sTmpViewDir);
				$this->sTmpViewDir = '';
			}
		}

		/**
		 * Add the CSS necessary for correct display of the Overview lists
		 * Can be called from any controller extending the Core\CmsController
		 * aCSS is rendered in the renderHead() function
		 */
		public function addOverviewCSS() {
			$this->aCss[] = 'public/modules/page/cms/css/chani/overview.css';
		}

		//TODO: Change URLs to parameters instead of hardcoding for more flexibility and provide default fallback
		/**
		 * @param $sModule
		 * Add the Javascript necessary for interaction with the Overview list
		 */
		public function addOverviewJS($sModule) {
			$this->aScripts[] = 'public/core/js/cms/chani/Overview.js';
			$this->aDomready[] = '
				'.ucfirst($sModule).'Overview = new chaniOverview({
					changeSequenceUrl: \'/chani/'.strtolower($sModule).'/changeSequence\',
					refreshOverviewUrl: \'/chani/'.strtolower($sModule).'/refreshOverview\'
				});
			';
		}

		/**
		 * @param string $sNameSpace
		 * @return Translator
		 * //Loads the translation file for the calling module controller in the user's language
		 */
		 //TODO: Tweak this function to handle non-existing translation locales and force it to use 'nl' instead of 'nl-NL'
		public function _getTranslation($sNameSpace = '')
		{

			//Determine the browser language of the user
			$sLanguage = $this->request->getBestLanguage();
			//Format returned for example: en-EN - we only need the first part
			$aLanguage = explode('-',$sLanguage);
			$sLanguage = $aLanguage[0];

			//If no namespace is provided, use the current one
			if($sNameSpace == '') {
				$sNameSpace = __NAMESPACE__;
			}

			//Build the regular path, the chani path and fallback (english) path using the namespace,
			$aNameSpace = explode('\\',$sNameSpace);
			if($aNameSpace[0] == 'Core') {
				$sPath = BASE_DIR;
			} else {
				$sPath = MODULE_DIR;
			}
			$sPath = $sPath.'/'.strtolower($aNameSpace[0]).'/translations/';
			$sChaniPath = $sPath.'chani/';
			$sFallback = $sChaniPath.'en.php';
			$sPath .= $sLanguage.'.php';
			$sChaniPath .= $sLanguage.'.php';
			//Check if we have a translation file for that language, if not fall back to whatever is available
			$aMessages = null;
			if (file_exists($sPath)) {
				require $sPath;
			} elseif(file_exists($sChaniPath)) {
				// fallback to some default
				require $sChaniPath;
			} else {
				require $sFallback;
			}

			//Return a translation object
			return new Translator(array("content" => $aMessages));

		}

	}
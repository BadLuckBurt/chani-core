<?php

	namespace Core;

	use Phalcon\Loader,
	Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter,
	Phalcon\Mvc\Dispatcher,
	Phalcon\Mvc\View\Simple,
	Phalcon\Events\Manager;

	class ModuleTemplate {

		public function registerAutoloaders() {}
		/**
		 * Register specific services for the module
		 */
		public function registerServices($oDi, $sNameSpace = '', $bCore = true)
		{
			$sFallback = '\Chani';
			$sDefaultNameSpace = $sNameSpace.'\Controllers';

			//Create a dispatcher to handle routed actions
			$oDispatcher = new Dispatcher();

			//Check if the module's controller has been extended, if not, fall back to the default controller
			if(class_exists($sDefaultNameSpace.'\AppController') === true) {
				$oDispatcher->setDefaultNamespace($sDefaultNameSpace);
			} else {
				$oDispatcher->setDefaultNamespace($sDefaultNameSpace.$sFallback);
			}
			$oDi->set('dispatcher', $oDispatcher);

			//Use the namespace to construct the path, the Core sits directly in the root
			//but the separate modules are in the module dir so alter the path if necessary
			if($bCore === true) {
				$sDir = __DIR__;
			} else {
				$sDir = MODULE_DIR.'/'.strtolower($sNameSpace);
			}

			//Register a simple volt compiler / renderer
			$oView = new Simple();
			$oView->setViewsDir($sDir.'/views/');
			$oView->registerEngines(array(
				".volt" => 'Phalcon\Mvc\View\Engine\Volt'
			));
			$oDi->set('view', $oView);

			//Create the database connection
			$aConfig = $oDi->get('config');
			$oDi->set('db', function () use ($aConfig) {
				return new DbAdapter(array(
					'host' => $aConfig->database->local->host,
					'username' => $aConfig->database->local->username,
					'password' => $aConfig->database->local->password,
					'dbname' => $aConfig->database->local->dbname,
					'charset'   =>'utf8'
				));
			});
		}
	}
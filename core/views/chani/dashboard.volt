<div id="dashboard-content-site" class="dashboard-content site">
    {{ content }}
</div>


{% for key, content in dashboardContents %}
<div id="dashboard-content-{{ key }}" class="dashboard-content cms collapse">
    <div class="dashboard-content-wrapper">
        {{ content }}
    </div>
</div>
{% endfor  %}

<div class="dashboard-nav" id="dashboard-nav">
    {{ nav }}
</div>

<div id="dashboard-overlay" class="dashboard-overlay" style="display: none;"></div>

{% for key, content in dashboardModals %}
<div id="dashboard-modal-{{ key }}" class="dashboard-modal" style="display: none;">
    <div class="modal-content">
        {{ content['html'] }}
    </div>

</div>
{% endfor %}

{%  if 2 == 3 %}
<div class="chaniTop bg900 noHover">
	<div class="chaniTopMenu">
		<div class="chaniLogo">
			<img src="{{url('public/modules/page/cms/media/chaniMini.png')}}" title="Chani CMS" />
		</div>
		{% if userIsLoggedIn == true %}
		<ul>
			<li class="bg800"><a href="{{url('cms/page')}}">Pages</a></li>
			<li class="bg800"><a href="{{url('cms/pizza')}}">Pizza</a></li>
			<li class="bg800"><a href="{{url('cms/form')}}">Form</a></li>
			<li class="bg800"><a href="{{url('cms/arma')}}">Arma</a></li>
			<li class="bg800"><a href="{{url('cms/vihana')}}">Vihana</a></li>
			<li class="bg800"><a href="{{url('cms/module')}}">Modules</a></li>
		</ul>
		<ul class="right">
			{% if userId == 1%}
			<li class="bg800"><a href="{{url('cms/user/profile/'~userId)}}">User: {{userName}}</a></li>
			{% endif %}
			<li class="bg800"><a href="{{url('cms/logout')}}">Log out</a></li>
		</ul>
		<div class="clear"></div>
		{% endif %}
	</div>
	<div class="clear"></div>
</div>
<div class="chaniWrapper bg500">
	<div class="chaniContentWrapper bg400">
		<div class="chaniContent">
		{{ content }}
		</div>
	</div>
	<div class="clear"></div>
</div>
{% if addPreview == true %}
<div id="previewOverlay" class="previewOverlay">
	<div class="previewOverlayBG"></div>
	<div id="previewOverlayContent" class="previewOverlayContent wrapper"></div>
	<div class="clear"></div>
</div>
{% endif%}
{% endif %}
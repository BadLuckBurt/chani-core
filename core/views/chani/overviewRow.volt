<div>
    {% if button['bAdd'] === true %}
    <span title="{{ t._('add') }}" data-id="{{ item['id'] }}" class="{{ module }}-add" data-target="add{{ module }}"><!--onclick="return Chani.toggleModalContent(this, true, true,event);">-->
        <i class="fa fa-2x fa-plus"></i></span>
    {% endif %}
    {% if button['bMove'] === true %}
    <span data-id="{{ item['id'] }}" class="{{ module }}-move" title="{{ t._('move') }}"><i class="fa fa-2x fa-arrows"></i></span>
    {% endif %}
    {% if count > 0 %}
    <span class="{{ module }}-sub" data-target="sub-sortables-{{ item['id'] }}" title="Toggle sub pages">
        <i class="fa fa-2x fa-bars"></i></span>
    {% endif %}
	<span class="title"><a id="{{ item['htmlId'] }}" href="{{ url(item['editUrl']) }}" title="Edit {{ item['sTitle'] }}">{{ item['sTitle'] }}</a></span>
    {% if button['bDelete'] === true %}
    <span data-id="{{ item['id'] }}" title="{{ t._('delete') }}" class="{{ module }}-delete floatRight" data-confirm="{{item['confirmDelete']}}?">
        <i class="fa fa-2x fa-trash"></i></span>
    {% endif %}
</div>
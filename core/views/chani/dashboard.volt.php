<div id="dashboard-content-site" class="dashboard-content site">
    <?php echo $content; ?>
</div>


<?php foreach ($dashboardContents as $key => $content) { ?>
<div id="dashboard-content-<?php echo $key; ?>" class="dashboard-content cms collapse">
    <div class="dashboard-content-wrapper">
        <?php echo $content; ?>
    </div>
</div>
<?php } ?>

<div class="dashboard-nav" id="dashboard-nav">
    <?php echo $nav; ?>
</div>

<div id="dashboard-overlay" class="dashboard-overlay" style="display: none;"></div>

<?php foreach ($dashboardModals as $key => $content) { ?>
<div id="dashboard-modal-<?php echo $key; ?>" class="dashboard-modal" style="display: none;">
    <div class="modal-content">
        <?php echo $content['html']; ?>
    </div>

</div>
<?php } ?>

<?php if (2 == 3) { ?>
<div class="chaniTop bg900 noHover">
	<div class="chaniTopMenu">
		<div class="chaniLogo">
			<img src="<?php echo $this->url->get('public/modules/page/cms/media/chaniMini.png'); ?>" title="Chani CMS" />
		</div>
		<?php if ($userIsLoggedIn == true) { ?>
		<ul>
			<li class="bg800"><a href="<?php echo $this->url->get('cms/page'); ?>">Pages</a></li>
			<li class="bg800"><a href="<?php echo $this->url->get('cms/pizza'); ?>">Pizza</a></li>
			<li class="bg800"><a href="<?php echo $this->url->get('cms/form'); ?>">Form</a></li>
			<li class="bg800"><a href="<?php echo $this->url->get('cms/arma'); ?>">Arma</a></li>
			<li class="bg800"><a href="<?php echo $this->url->get('cms/vihana'); ?>">Vihana</a></li>
			<li class="bg800"><a href="<?php echo $this->url->get('cms/module'); ?>">Modules</a></li>
		</ul>
		<ul class="right">
			<?php if ($userId == 1) { ?>
			<li class="bg800"><a href="<?php echo $this->url->get('cms/user/profile/' . $userId); ?>">User: <?php echo $userName; ?></a></li>
			<?php } ?>
			<li class="bg800"><a href="<?php echo $this->url->get('cms/logout'); ?>">Log out</a></li>
		</ul>
		<div class="clear"></div>
		<?php } ?>
	</div>
	<div class="clear"></div>
</div>
<div class="chaniWrapper bg500">
	<div class="chaniContentWrapper bg400">
		<div class="chaniContent">
		<?php echo $content; ?>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php if ($addPreview == true) { ?>
<div id="previewOverlay" class="previewOverlay">
	<div class="previewOverlayBG"></div>
	<div id="previewOverlayContent" class="previewOverlayContent wrapper"></div>
	<div class="clear"></div>
</div>
<?php } ?>
<?php } ?>
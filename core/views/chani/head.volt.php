<?php foreach ($meta as $name => $values) { ?>
    <?php foreach ($values as $key => $value) { ?>
    <meta <?php echo $name; ?>="<?php echo $key; ?>" content="<?php echo $value; ?>" />
    <?php } ?>
<?php } ?>
<!-- CSS -->
<?php foreach ($css as $sheet) { ?>
    <link href="<?php echo $this->url->get($sheet); ?>" rel="stylesheet" />
<?php } ?>
<!-- CSS inline -->
<style type="text/css">
    <?php foreach ($cssInline as $inline) { ?>
    <?php echo $inline; ?>
    <?php } ?>
</style>
<!-- Scripts -->
<?php foreach ($scripts as $script) { ?>
	<?php echo $this->tag->javascriptInclude($script); ?>
<?php } ?>
<!-- Domready events -->
<script type="text/javascript">
	window.addEvent('domready', function() {
	<?php foreach ($domready as $code) { ?>
		<?php echo $code; ?>
	<?php } ?>
	});

</script>
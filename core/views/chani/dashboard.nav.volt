<nav>
    <ul>
        <li title="Show / hide full menu">
            <i class="fa fa-2x fa-eye"></i>
            <span>Chani</span>
        </li>
        {% for module in modules %}
            <li>
                <a href="{{ url(module['href']) }}" id="nav-{{ module['id'] }}" title="{{ module['title'] }}" class="mainNav {{ module['class'] }}">
                    <i class="fa fa-2x {{ module['iconClass'] }}"></i>
                    <span>{{ module['innerHTML'] }}</span>
                </a>
                <ul id="nav-{{ module['id'] }}-sub" class="{{ module['class'] }}">
                    {% for sub in module['sub'] %}
                        <li><a href="{{ url(sub['href']) }}" class="{{ module['class'] }}" id="{{ sub['id'] }}" data-target="{{ sub['target'] }}" title="{{ sub['title'] }}">
                                <i class="fa fa-2x {{ sub['class'] }}"></i>
                                <span>{{ sub['title'] }}</span>
                            </a>
                        </li>
                    {% endfor %}
                </ul>
            </li>
        {% endfor %}
        <li>&nbsp;</li>
        <li><a class="mainNav logout" title="Log out" href="/chani/logout">
                <i class="fa fa-2x fa-sign-out fa-rotate-180"></i>
                <span>Log out</span>
            </a></li>
    </ul>
</nav>
<!DOCTYPE html>
<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Lato|Shanti' rel='stylesheet' type='text/css'>
	<title>
		
		<?php echo (empty($title) ? ('No title') : ($title)); ?>
		
	</title>
	<link href="<?php echo $this->url->get('public/core/media/favicon.ico'); ?>" rel="shortcut icon" type="image/x-icon" />
	<!--Enter the head scripts and stylesheet here-->
	
	<?php echo (empty($head) ? ('') : ($head)); ?>
	
</head>
<body class="expand-nav">
	<!-- Enter the body code / output here -->
	
	<?php echo (empty($body) ? ('') : ($body)); ?>
	
</body>
</html>
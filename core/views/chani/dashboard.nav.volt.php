<nav>
    <ul>
        <li title="Show / hide full menu">
            <i class="fa fa-2x fa-eye"></i>
            <span>Chani</span>
        </li>
        <?php foreach ($modules as $module) { ?>
            <li>
                <a href="<?php echo $this->url->get($module['href']); ?>" id="nav-<?php echo $module['id']; ?>" title="<?php echo $module['title']; ?>" class="mainNav <?php echo $module['class']; ?>">
                    <i class="fa fa-2x <?php echo $module['iconClass']; ?>"></i>
                    <span><?php echo $module['innerHTML']; ?></span>
                </a>
                <ul id="nav-<?php echo $module['id']; ?>-sub" class="<?php echo $module['class']; ?>">
                    <?php foreach ($module['sub'] as $sub) { ?>
                        <li><a href="<?php echo $this->url->get($sub['href']); ?>" class="<?php echo $module['class']; ?>" id="<?php echo $sub['id']; ?>" data-target="<?php echo $sub['target']; ?>" title="<?php echo $sub['title']; ?>">
                                <i class="fa fa-2x <?php echo $sub['class']; ?>"></i>
                                <span><?php echo $sub['title']; ?></span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </li>
        <?php } ?>
        <li>&nbsp;</li>
        <li><a class="mainNav logout" title="Log out" href="/chani/logout">
                <i class="fa fa-2x fa-sign-out fa-rotate-180"></i>
                <span>Log out</span>
            </a></li>
    </ul>
</nav>
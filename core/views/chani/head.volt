{% for name, values in meta %}
    {% for key, value in values %}
    <meta {{name}}="{{key}}" content="{{value}}" />
    {% endfor %}
{% endfor %}
<!-- CSS -->
{% for sheet in css %}
    <link href="{{ url(sheet) }}" rel="stylesheet" />
{% endfor %}
<!-- CSS inline -->
<style type="text/css">
    {% for inline in cssInline %}
    {{ inline }}
    {% endfor %}
</style>
<!-- Scripts -->
{% for script in scripts %}
	{{ javascript_include(script) }}
{% endfor %}
<!-- Domready events -->
<script type="text/javascript">
	window.addEvent('domready', function() {
	{% for code in domready %}
		{{ code }}
	{% endfor %}
	});

</script>
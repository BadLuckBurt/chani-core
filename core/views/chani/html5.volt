<!DOCTYPE html>
<html>
<head>
	<link href='http://fonts.googleapis.com/css?family=Lato|Shanti' rel='stylesheet' type='text/css'>
	<title>
		{% block titleTag %}
		{{title|default('No title')}}
		{% endblock %}
	</title>
	<link href="{{ url('public/core/media/favicon.ico')}}" rel="shortcut icon" type="image/x-icon" />
	<!--Enter the head scripts and stylesheet here-->
	{% block headTag %}
	{{head|default('')}}
	{% endblock %}
</head>
<body class="expand-nav">
	<!-- Enter the body code / output here -->
	{% block bodyTag %}
	{{body|default('')}}
	{% endblock %}
</body>
</html>
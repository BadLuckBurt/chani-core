<div>
    <?php if ($button['bAdd'] === true) { ?>
    <span title="<?php echo $t->_('add'); ?>" data-id="<?php echo $item['id']; ?>" class="<?php echo $module; ?>-add" data-target="add<?php echo $module; ?>"><!--onclick="return Chani.toggleModalContent(this, true, true,event);">-->
        <i class="fa fa-2x fa-plus"></i></span>
    <?php } ?>
    <?php if ($button['bMove'] === true) { ?>
    <span data-id="<?php echo $item['id']; ?>" class="<?php echo $module; ?>-move" title="<?php echo $t->_('move'); ?>"><i class="fa fa-2x fa-arrows"></i></span>
    <?php } ?>
    <?php if ($count > 0) { ?>
    <span class="<?php echo $module; ?>-sub" data-target="sub-sortables-<?php echo $item['id']; ?>" title="Toggle sub pages">
        <i class="fa fa-2x fa-bars"></i></span>
    <?php } ?>
	<span class="title"><a id="<?php echo $item['htmlId']; ?>" href="<?php echo $this->url->get($item['editUrl']); ?>" title="Edit <?php echo $item['sTitle']; ?>"><?php echo $item['sTitle']; ?></a></span>
    <?php if ($button['bDelete'] === true) { ?>
    <span data-id="<?php echo $item['id']; ?>" title="<?php echo $t->_('delete'); ?>" class="<?php echo $module; ?>-delete floatRight" data-confirm="<?php echo $item['confirmDelete']; ?>?">
        <i class="fa fa-2x fa-trash"></i></span>
    <?php } ?>
</div>